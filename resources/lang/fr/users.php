<?php

return [
    'title' => 'Utilisateurs',
    'edit' => "Modifier l'article: :attribute",
    'createButton' => 'Ajouter une',
    'create' => 'Nouvel employé',
    'list' => [
        'fio' => 'Nom et prénom',
        'organization' => "Nom de l'entreprise",
        'surname' => 'Nom de famille',
        'first_name' => 'Nom',
        'middle_name' => 'Deuxième nom',
        'address' => 'Adresse',
        'about_me' => 'Sur moi',
        'email' => 'E-mail',
        'date_of_birth' => 'Date de naissance',
        'start_date_job' => "Date d'embauche",
        'status' => 'Statut',
        'phone' => 'Téléphoner',
        'roles' => 'Les rôles',
    ],
];
