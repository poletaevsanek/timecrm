<?php

return [
    'title' => 'Les rôles',
    'createButton' => 'Nouveau rôle',
    'create' => 'Nouveau rôle',
    'edit' => 'Modifier le rôle :attribute',
    'abilities' => [
        'read' => 'En train de lire',
        'edit' => 'Le changement',
        'delete' => 'Suppression'
    ],
    'back' => 'Annulation',
];
