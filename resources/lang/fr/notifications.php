<?php

return [
    'delete' => "Voulez-vous vraiment supprimer l'élément? Tous les éléments associés seront supprimés.",
    'error_body' => "Une erreur s'est produite lors du changement. Essayer plus tard. ",
    'error_title' => 'Erreur',
    'smoke_pure_yes' => 'Oui',
    'smoke_pure_no' => 'Annuler',
    'editLanguage' => "Voulez-vous vraiment changer la langue? Toutes les modifications apportées sur cette page ne seront pas enregistrées.",
];
