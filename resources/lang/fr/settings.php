<?php

return [
    'edit' => 'Paramètres',
    'list' => [
        'name' => 'Nom du système',
        'logo_file' => 'Logo',
        'bg_file' => 'Image de fond'
    ]
];
