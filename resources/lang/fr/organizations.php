<?php

return [
    'title' => 'Liste des organisations',
    'createButton' => 'Ajouter une organisation',
    'list' => [
        'id' => 'ID',
        'name' => 'Nom',
        'timezone' => 'Fuseau horaire',
        'timezone_id' => 'Fuseau horaire',

        'header_admin' => 'Données administrateur',
        'first_name' => 'Nom',
        'surname' => 'Nom de famille',
        'middle_name' => 'Second prénom',
        'email' => 'E-mail',
        'password' => 'Mot de passe',
        'password_confirm' => 'Confirmer le mot de passe',
        'branch' => 'Branches',
        'logo' => 'Image',
        'count' => "Nombre d'employés",
        'date' => "Date d'enregistrement"
    ],
    'create' => 'Nouvelle organisation',
    'edit' => "Modifier l'article: :attribute",
];
