<?php

return [
    'failed' => 'Ces informations d\'identification ne correspondent pas à nos enregistrements.',
    'password' => 'Le mot de passe spécifié n\'est pas valide.',
    'throttle' => 'Trop de tentatives de connexion. Veuillez réessayer dans :seconds secondes. ',
];
