<?php

return [
    'title' => 'Liste des branches',
    'createButton' => 'Ajouter une branch',
    'list' => [
        'name' => 'Nom',
        'timezone' => 'Fuseau horaire',
    ],
    'create' => 'Nouvelle branch',
    'edit' => "Modifier l'article: :attribute",
    'back' => 'Cancellation',
];
