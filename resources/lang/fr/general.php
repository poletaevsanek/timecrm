<?php

return [
    'search' => 'Rechercher',
    'list' => [
        'id' => 'ID',
        'name' => 'Nom',
        'action' => 'Action'
    ],
    'create' => 'Sauvegarder',
    'edit' => 'Éditer',
    'back' => 'Annulation',
    'button' => [
        'photo' => 'Ajouter une image'
    ],
    'message' => [
        'errors' => [
            'body' => "Une erreur s'est produite lors de la tentative de contact avec le serveur",
            'title' => 'Erreur',
        ],
        'store' => [
            'body' => 'Élément :attribute créé avec succès',
            'title' => 'Succès',
        ],
        'update' => [
            'body' => 'Élément :attribute modifié avec succès',
            'title' => 'Succès',
        ],
    ],
];
