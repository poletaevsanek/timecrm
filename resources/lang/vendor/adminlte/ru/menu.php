<?php

return [
    'organizations' => 'Список организации',
    'settings' => 'Настройки',
    'user_organization' => 'Администраторы организаций',
    'users' => 'Сотрудники',
    'branches' => 'Филиалы',
    'all_settings' => 'Настройки',
    'roles' => 'Роли',
];
