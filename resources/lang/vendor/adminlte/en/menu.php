<?php

return [
    'organizations' => 'Organization list',
    'settings' => 'Settings',
    'user_organization' => 'Organization Administrators',
    'users' => 'Staff',
    'branches' => 'Branches',
    'all_settings' => 'Settings',
    'roles' => 'Roles',
];
