<?php

return [
    'organizations' => 'Liste des organisations',
    'settings' => 'Paramètres',
    'user_organization' => "Administrateurs d'organisation",
    'users' => 'Personnel',
    'branches' => 'Branches',
    'all_settings' => 'Paramètres',
    'roles' => 'Les rôles',
];
