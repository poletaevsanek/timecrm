<?php

return [
    'title' => 'Users',
    'edit' => 'Edit item: :attribute',
    'createButton' => 'Add',
    'create' => 'New employee',
    'list' => [
        'fio' => 'Full name',
        'organization' => 'Name of company',
        'surname' => 'Surname',
        'first_name' => 'Name',
        'middle_name' => 'Middle name',
        'address' => 'Address',
        'about_me' => 'About me',
        'email' => 'Email',
        'date_of_birth' => 'Date of Birth',
        'start_date_job' => 'Hiring date',
        'status' => 'Status',
        'phone' => 'Phone',
        'roles' => 'Roles',
    ],
];
