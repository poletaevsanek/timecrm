<?php

return [
    'search' => 'Search',
    'list' => [
        'id' => 'ID',
        'name' => 'Name',
        'action' => 'Action'
    ],
    'create' => 'Save',
    'edit' => 'Edit',
    'back' => 'Cancellation',
    'button' => [
        'photo' => 'Add image'
    ],
    'message' => [
        'errors' => [
            'body' => 'An error occurred while trying to contact the server',
            'title' => 'Error',
        ],
        'store' => [
            'body' => 'Element :attribute created successfully',
            'title' => 'Success',
        ],
        'update' => [
            'body' => 'Element :attribute changed successfully',
            'title' => 'Success',
        ],
    ],
];
