<?php

return [
    'delete' => 'Are you sure you want to delete the item? All related items will be removed.',
    'error_body' => 'An error occurred while changing. Try later.',
    'error_title' => 'Error',
    'smoke_pure_yes' => 'Yes',
    'smoke_pure_no' => 'Cancellation',
    'editLanguage' => 'Are you sure you want to change the language? All changes made on this page will not be saved.'
];
