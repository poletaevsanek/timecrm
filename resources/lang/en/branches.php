<?php

return [
    'title' => 'Branches',
    'createButton' => 'Add branch',
    'list' => [
        'name' => 'Name',
        'timezone' => 'Timezone',
    ],
    'create' => 'New branch',
    'edit' => 'Edit item: :attribute',
    'back' => 'Cancellation',
];
