<?php

return [
    'title' => 'Roles',
    'createButton' => 'New role',
    'create' => 'New role',
    'edit' => 'Edit Role :attribute',
    'abilities' => [
        'read' => 'Reading',
        'edit' => 'The change',
        'delete' => 'Deleting'
    ],
    'back' => 'Cancellation ',
];
