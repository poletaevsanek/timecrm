<?php

return [
    'title' => 'List of organizations',
    'createButton' => 'Add Organization',
    'list' => [
        'id' => 'ID',
        'name' => 'Name',
        'timezone' => 'Timezone',
        'timezone_id' => 'Timezone',

        'header_admin' => 'Administrator data',
        'first_name' => 'Name',
        'surname' => 'Last name',
        'middle_name' => 'Middle name',
        'email' => 'Email',
        'password' => 'Password',
        'password_confirm' => 'Confirm password',
        'branch' => 'Branches',
        'logo' => 'Image',
        'count' => 'Number of employees',
        'date' => 'Date of registration'
    ],
    'create' => 'New Organization',
    'edit' => 'Edit item: :attribute',
    'back' => 'Cancellation',
];
