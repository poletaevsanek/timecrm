<?php

return [
    'edit' => 'Settings',
    'list' => [
        'name' => 'System name',
        'logo_file' => 'Logo',
        'bg_file' => 'Background image'
    ]
];
