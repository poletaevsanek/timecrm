<?php

return [
    'title' => 'Список организаций',
    'createButton' => 'Добавить организацию',
    'list' => [
        'id' => 'ID',
        'name' => 'Наименование',
        'timezone' => 'Часовой пояс',
        'timezone_id' => 'Часовой пояс',
        'header_admin' => 'Данные администратора',
        'first_name' => 'Имя',
        'surname' => 'Фамилия',
        'middle_name' => 'Отчество',
        'email' => 'Email',
        'password' => 'Пароль',
        'password_confirm' => 'Подтверждение пароля',
        'branch' => 'Филиалы',
        'logo' => 'Изображение',
        'count' => 'Кол-во сотрудников',
        'date' => 'Дата регистрации'
    ],
    'create' => 'Новая организация',
    'edit' => 'Редактировать элемент: :attribute',
    'back' => 'Отмена',
];
