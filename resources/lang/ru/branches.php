<?php

return [
    'title' => 'Филиалы',
    'createButton' => 'Добавить филиал',
    'list' => [
        'name' => 'Наименование',
        'timezone' => 'Часовой пояс',
    ],
    'create' => 'Новый филиал',
    'edit' => 'Редактировать элемент: :attribute',
    'back' => 'Отмена',
];
