<?php

return [
    'title' => 'Пользователи',
    'edit' => 'Редактировать элемент: :attribute',
    'createButton' => 'Добавить',
    'create' => 'Новый сотрудник',
    'list' => [
        'fio' => 'ФИО',
        'organization' => 'Наименование организации',
        'surname' => 'Фамилия',
        'first_name' => 'Имя',
        'middle_name' => 'Отчество',
        'address' => 'Адрес',
        'about_me' => 'О себе',
        'email' => 'E-mail',
        'date_of_birth' => 'Дата рождения',
        'start_date_job' => 'Дата принятия на работу',
        'status' => 'Статус',
        'phone' => 'Телефон',
        'roles' => 'Роли',
    ],
];
