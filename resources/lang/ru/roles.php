<?php

return [
    'title' => 'Роли',
    'createButton' => 'Новая роль',
    'create' => 'Новая роль',
    'edit' => 'Редактировать роль :attribute',
    'abilities' => [
        'read' => 'Чтение',
        'edit' => 'Изменение',
        'delete' => 'Удаление'
    ],
    'back' => 'Отмена',
];
