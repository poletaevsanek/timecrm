@extends('adminlte::page')

@isset($title)
    @section('title', $title)

@section('content_header')
    <h1>{{ $title }}</h1>
@stop

@endisset

@section('content')
    @yield('content_general')
@stop


@section('adminlte_js')
    {!! Toastr::render() !!}
    <script>
        var notificationList ={!!   json_encode(__('notifications'),JSON_UNESCAPED_UNICODE) !!};
        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
                toastr.error("{{ $error }}");
            @endforeach
        @endif
        var token = '{{ csrf_token() }}';
    </script>
    @yield('js')
@stop
