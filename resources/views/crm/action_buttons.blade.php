@if(isset($onClick))
    @include('crm.onclick_button')
    @include('crm.delete_button')
@else
    @can($element->getTable() . '-edit')
        @include('crm.edit_button')
    @endcan
    @if(!isset($withoutDestroy) or (isset($withoutDestroy) and $withoutDestroy == false))
        @can($element->getTable() . '-delete')
            @include('crm.delete_button')
        @endcan
    @endif
@endif
