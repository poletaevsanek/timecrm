<a @if(isset($onClick ))  onclick="{{ $onClick }};"  href="javascript:void(0);"
   @else
    href="{{ route( $routeName . 'edit', $element->id) }}"
   @endif
   class="btn btn-xs btn-primary" style="margin-left: 10px;"
   title="{{ __('general.edit') }}"><i class="fas fa-edit" style="font-size: 17px;"></i></a>
