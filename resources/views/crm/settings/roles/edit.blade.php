@extends('crm.general')



@section('css')
    <link rel="stylesheet" href="{{ asset('css/settings.css') }}">
@stop

@section('content_general')
    <div class="row">
        <div class="col-12">

            <!-- /.card-header -->
            <form method="POST" id="list-file-form" enctype="multipart/form-data"
                  role="form" action="{{ $route }}">
                @if(!empty($model))
                    {{ method_field('PATCH') }}
                @endif
                @csrf
                <div class="row">
                    <div class="col-auto col-sm-auto col-md-auto col-lg-12 col-xl-12">
                        <div class="card">
                            <div class="card-header">
                                <ul class="nav nav-tabs" id="translationTab" role="tablist">
                                    @foreach($languages as $item)
                                        <li class="nav-item" id="{{$item->name}}">
                                            <a class="nav-link
                                        @if(!empty($model))
                                            {{$model->language_id == $item->id ? 'active' : ''}}
                                            @else
                                            {{Auth::user()->language_value_id == $item->id ? 'active' : ''}}
                                            @endif"
                                               id="{{ $item->name }}-tab" data-toggle="tab"
                                               href="#content-{{$item->name}}" role="tab" aria-controls="home"
                                               aria-selected="true">
                                        <span><img src="{{asset($item->icon)}}"
                                                   style="width: 32px;"> {{$item->name}}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="translationTabContent">
                                    @foreach($languages as $item)
                                        {{ App::setLocale($item->name) }}
                                        <div class="tab-pane fade show
                            @if(!empty($model))
                                        {{$model->language_id == $item->id ? 'active' : ''}}
                                        @else
                                        {{Auth::user()->language_value_id == $item->id ? 'active' : ''}}
                                        @endif"
                                             id="content-{{$item->name}}" role="tabpanel" aria-labelledby="home-tab">
                                            <div class="row">
                                                <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                                                    <div class="form-group">
                                                        <label>{{ __('general.list.name') }}</label>
                                                        <input name="{{$item->name}}[name]"
                                                               placeholder="{{ __('general.list.name') }}"
                                                               @if(!empty($model))
                                                               value="{{$model->paramOutput($item, 'display_name')}}"
                                                               @else
                                                               value="{{ old($item->name.'name') }}"
                                                               @endif
                                                               class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{ App::setlocale(Auth::user()->languageValue->name) }}
                                    @endforeach
                                </div>
                                <table class="table table-hover table-sm">
                                    <thead>
                                    <tr>
                                        <th>Страница</th>
                                        <th class="text-center">
                                            <input type="checkbox" class="form-control" id="read" data-checbox="read" data-checked="true">
                                            <label for="edit"></label>
                                            {{ __('roles.abilities.read') }}</th>
                                        <th class="text-center">
                                            <input type="checkbox" class="form-control" id="edit" data-checbox="edit" data-checked="true">
                                            <label for="edit"></label>
                                            {{ __('roles.abilities.edit') }}</th>
                                        <th class="text-center">
                                            <input type="checkbox" class="form-control" id="delete" data-checbox="delete" data-checked="true">
                                            <label for="delete"></label>
                                            {{ __('roles.abilities.delete') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach( $menu as $item)
                                        @if(isset($item['text']) && !empty($item['text']))
                                            @if(!isset($item['superadmin']) || ($item['text'] == 'branches' && Auth::user()->organization->branch))
                                                @isset($item['can'])
                                            @include('crm.settings.roles.menu', ['item' => $item])
                                            @isset($item['submenu'])
                                                @foreach( $item['submenu'] as $submenu)
                                                    @isset($submenu['can'])
                                                        @include('crm.settings.roles.menu', ['item' => $submenu,
                                                                                            'menuClass' => 'settings_second_menu',
                                                                                             'dataParent' => $item['text']
                                                                                             ])
                                                    @endisset
                                                @endforeach
                                            @endisset
                                        @endisset
                                            @endif
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @include('crm.general_edit_footer')
                        </div>
                    </div>
                </div>
            </form>
            <!-- /.card-body -->
        </div>
    </div>
@stop
@section('js')
<script>
    $('[data-checked="true"]').on('change',function (){
        let element = $(this);
       $.each($('input'), function (item, index){
          if($(this).val() == element.data('checbox')){
              if(element.prop("checked")){
                  $(this).attr("checked","checked");
              } else {
                  $(this).removeAttr("checked");
              }
          }
       });
    });

</script>
@stop
