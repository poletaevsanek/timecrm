<tr>
    <td class="settings_menu_td">
        <div class="@isset($menuClass) {{ $menuClass }} @endisset">
            <i class=" @isset($item['icon']){{ $item['icon'] }} @else far fa-fw fa-circle  @endisset"></i>
            @lang('adminlte::menu.' . $item['text'])
        </div>
    </td>
    <td class="text-center settings_menu_td">
        <div class="settings_menu_checkbox" title="{{ __('settings.roles.abilities.read') }}">
            <input type="checkbox" class="form-control" id="{{ $item['text'] . 'read' }}"
                   name="permissions[{{ $item['text'] }}][read]" value="read"
                   @if(isset($model) and $model->hasPermissionTo($item['text'] .'-'. 'read')) checked @endif>
            @isset($dataParent)
                <input type="hidden" name="permissions[{{ $item['text'] }}][parent]" value="{{ $dataParent }}">
            @endisset
            <label for="{{ $item['text'] . 'read' }}"></label>
        </div>
    </td>
    <td class="text-center settings_menu_td">
        @if(!isset($item['submenu']))
            <div class="settings_menu_checkbox" title="{{ __('settings.roles.abilities.edit') }}">
                <input type="checkbox" class="form-control" id="{{ $item['text'] . 'edit' }}"
                       name="permissions[{{ $item['text']}}][edit]" value="edit"
                       @if(isset($model) and $model->hasPermissionTo($item['text'] .'-'. 'edit')) checked @endif>
                <label for="{{ $item['text'] . 'edit' }}"></label>
            </div>
        @endif
    </td>
    <td class="text-center settings_menu_td">
        @if(!isset($item['submenu']))
            <div class="settings_menu_checkbox" title="{{ __('settings.roles.abilities.delete') }}">
                <input type="checkbox" class="form-control" id="{{ $item['text'] . 'delete' }}"
                       name="permissions[{{ $item['text'] }}][delete]" value="delete"
                       @if(isset($model) and $model->hasPermissionTo($item['text'] .'-'. 'delete')) checked @endif>
                <label for="{{ $item['text'] . 'delete' }}"></label>
            </div>
        @endif
    </td>
</tr>
