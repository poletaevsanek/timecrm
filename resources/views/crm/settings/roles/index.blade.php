@extends('crm.general')

@section('content_general')
    <div class="card">
        <!-- /.card-header -->
        <div class="card-header">
            @include('crm.header-page')
        </div>
        <div class="card-body">
            <div class="p-0">
                {!! $datatable->table() !!}
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop
@section('js')
    {!! $datatable->scripts() !!}
    <script>
        $('input[data-type="table_search"]').keyup(function (event) {
            $('#dataTableBuilder').DataTable().search($(this).val()).ajax.reload(null, false);
        });
    </script>
@stop

