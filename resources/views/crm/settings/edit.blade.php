@extends('crm.general')

@section('content_general')
    <div class="card">
        <form method="POST" enctype="multipart/form-data"
              role="form" action="{{ $route }}" id="form" data-attr-lang="yes"
              onsubmit="Main.sendFormData(event, $(this))">
            @if(!empty($model))
                {{ method_field('PATCH') }}
            @endif
            @csrf
            <div class="card-body row">
                <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                    <div class="form-group  justify-content-center d-flex">
                        <img src="{{ $model->logo_photo() }}"
                             style="width: 145px; height: 145px;"
                             class="elevation-2" data-logo="true"
                             alt="{{ __($translation . 'list.logo_file') }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-block btn-primary" type="button"
                                onclick="$('#logoFile').click()">{{ __($translation . 'list.logo_file') }}</button>
                        <input type="file" class="d-none" name="logo_file" id="logoFile">
                    </div>
                </div>

                <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                    <div class="form-group  justify-content-center d-flex">
                        <img src="{{ $model->bg_photo() }}"
                             style="width: 145px; height: 145px;"
                             class="elevation-2" data-bg="true"
                             alt="{{  __($translation . 'list.bg_file') }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-block btn-primary" type="button"
                                onclick="$('#bgFile').click()">{{ __($translation . 'list.bg_file') }}</button>
                        <input type="file" class="d-none" name="bg_file" id="bgFile">
                    </div>
                </div>
            </div>
            <div class="card-header">
                <ul class="nav nav-tabs" id="translationTab" role="tablist">
                    @foreach($languages as $item)
                        <li class="nav-item" id="{{$item->name}}">
                            <a class="nav-link
                                        @if(!empty($model))
                            {{$model->language_id == $item->id ? 'active' : ''}}
                            @else
                            {{Auth::user()->language_value_id == $item->id ? 'active' : ''}}
                            @endif"
                               id="{{ $item->name }}-tab" data-toggle="tab"
                               href="#content-{{$item->name}}" role="tab" aria-controls="home"
                               aria-selected="true">
                                        <span><img src="{{asset($item->icon)}}"
                                                   style="width: 32px;"> {{$item->name}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="translationTabContent">
                    @foreach($languages as $item)
                        {{ App::setLocale($item->name) }}
                        <div class="tab-pane fade show
                            @if(!empty($model))
                        {{$model->language_id == $item->id ? 'active' : ''}}
                        @else
                        {{Auth::user()->language_value_id == $item->id ? 'active' : ''}}
                        @endif"
                             id="content-{{$item->name}}" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                                    <div class="form-group">
                                        <label>{{ __($translation . 'list.name') }}</label>
                                        <input name="{{$item->name}}[system_name]"
                                               placeholder="{{ __($translation . 'list.name') }}"
                                               @if(!empty($model))
                                               value="{{$model->paramOutput($item, 'system_name')}}"
                                               @else
                                               value="{{ old($item->name.'system_name') }}"
                                               @endif
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ App::setlocale(Auth::user()->languageValue->name) }}
                    @endforeach
                </div>
            </div>
                @include('crm.general_edit_footer', ['not_back'=> true])
        </form>
    </div>
@stop
@section('js')
    <script src="{{asset('/js/settings/edit.js')}}"></script>
@stop
