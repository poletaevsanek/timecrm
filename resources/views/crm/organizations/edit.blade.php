@extends('crm.general')

@section('content_general')
    <div class="card">
        <form method="POST" enctype="multipart/form-data"
              role="form" action="{{ $route }}" id="form" data-attr-lang="yes"
              onsubmit="Main.sendFormData(event, $(this))">
            @if(!empty($model))
                {{ method_field('PATCH') }}
            @endif
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                        <div class="form-group  justify-content-center d-flex">
                            <img src="{{ !empty($model) ? $model->logo_image() : asset('/img/stub.png') }}"
                                 style="width: 183px;height: 183px;"
                                 class="user-image img-circle elevation-2" data-logo="true"
                                 alt="{{!empty($model) ? $model->name : ''}}">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-block btn-primary" type="button"
                                    onclick="$('#file').click()">{{ __('general.button.photo') }}</button>
                            <input type="file" class="d-none" name="logo" id="file">
                        </div>
                    </div>
                    <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                        <div class="form-group">
                            <label>{{ __($translation . 'list.timezone') }}</label>
                            <select name="timezone_id" data-language="{{ Auth::user()->languageValue->name }}"
                                    class="form-control select2" style="width: 100%;"
                                    data-placeholder="{{ __($translation . 'list.timezone') }}">
                                @if(!$timezones->isEmpty())
                                    <option></option>
                                    @foreach($timezones as $timezone)
                                        <option value="{{$timezone->id}}"
                                                @if(!empty($model) && $model->timezone_id == $timezone->id) selected @endif>
                                            {{$timezone->name}}/({{$timezone->timezone}})
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="branches" name="branch" @if(!empty($model) && $model->branch) checked @endif>
                                <label for="branches"
                                       class="custom-control-label">{{ __($translation . 'list.branch') }}</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-header">
                <ul class="nav nav-tabs" id="translationTab" role="tablist">
                    @foreach($languages as $item)
                        <li class="nav-item" id="{{$item->name}}">
                            <a class="nav-link
                                        @if(!empty($model))
                            {{$model->language_id == $item->id ? 'active' : ''}}
                            @else
                            {{Auth::user()->language_value_id == $item->id ? 'active' : ''}}
                            @endif"
                               id="{{ $item->name }}-tab" data-toggle="tab"
                               href="#content-{{$item->name}}" role="tab" aria-controls="home"
                               aria-selected="true">
                                        <span><img src="{{asset($item->icon)}}"
                                                   style="width: 32px;"> {{$item->name}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="translationTabContent">
                    @foreach($languages as $item)
                        {{ App::setLocale($item->name) }}
                        @include('crm.organizations.form')
                    @endforeach
                    {{ App::setlocale(Auth::user()->languageValue->name) }}
                </div>
            </div>
            @include('crm.general_edit_footer')
        </form>
    </div>
@stop
@section('js')
    <script src="{{ asset('/js/organizations/edit.js') }}"></script>
@stop
