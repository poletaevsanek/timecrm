    <div class="tab-pane fade show
     @if(!empty($model))
    {{$model->language_id == $item->id ? 'active' : ''}}
    @else
    {{Auth::user()->language_value_id == $item->id ? 'active' : ''}}
    @endif"
         id="content-{{$item->name}}" role="tabpanel" aria-labelledby="home-tab">
        <div class="row">
            <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                <div class="form-group">
                    <label>{{ __($translation . 'list.name') }}</label>
                    <input name="{{$item->name}}[name]"
                           placeholder="{{ __($translation . 'list.name') }}"
                           @if(!empty($model))
                           value="{{$model->paramOutput($item, 'name')}}"
                           @else
                           value="{{ old($item->name.'name') }}"
                           @endif
                           class="form-control">
                </div>
            </div>
        </div>
        @if(empty($model))
            <hr/>
            <div class="form-group">
                <h3>{{  __($translation . 'list.header_admin') }}</h3>
            </div>
            <div class="row">
                <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label>{{ __($translation . 'list.first_name') }}</label>
                        <input class="form-control" type="text" name="{{$item->name}}[first_name]"
                               placeholder="{{ __($translation . 'list.first_name') }}">
                    </div>
                </div>
                <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label>{{ __($translation . 'list.surname') }}</label>
                        <input class="form-control" type="text" name="{{$item->name}}[surname]"
                               placeholder="{{ __($translation . 'list.surname') }}">
                    </div>
                </div>
            </div>
            @if($item->id == Auth::user()->language_value_id)
                <div class="row">
                    <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                        <div class="form-group">
                            <label>{{ __($translation . 'list.email') }}</label>
                            <input class="form-control" type="email" name="{{$item->name}}[email]"
                                   placeholder="{{__($translation . 'list.email') }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                        <div class="form-group">
                            <label>{{ __($translation. 'list.password') }}</label>
                            <input class="form-control" type="password" name="{{$item->name}}[password]"
                                   placeholder="{{ __($translation . 'list.password') }}">
                        </div>
                    </div>
                    <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                        <div class="form-group">
                            <label>{{ __($translation . 'list.password_confirm') }}</label>
                            <input class="form-control" type="password" name="{{$item->name}}[password_confirmation]"
                                   placeholder="{{ __($translation . 'list.password_confirm') }}">
                        </div>
                    </div>
                </div>
            @endif
        @endif
    </div>
