<div class="card-footer">
    <button class="btn btn-success" type="submit">{{ !empty($model) ? __('general.edit') : __('general.create')}}</button>
    @if(!isset($not_back))
        <a href="{{ $routeBack }}" class="btn btn-danger float-right">{{ __('general.back') }}</a>
    @endif
</div>
