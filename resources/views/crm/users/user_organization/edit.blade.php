@extends('crm.general')

@section('content_general')
    <div class="card">
        <form method="POST" enctype="multipart/form-data"
              data-attr-lang="yes"
              role="form" action="{{ $route }}" id="form">
            {{ method_field('PATCH') }}
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                        <div class="form-group">
                            <label>{{ __( 'organizations.list.email') }}</label>
                            <input class="form-control" type="email" name="email"
                                   value="{{ $model->email }}"
                                   placeholder="{{__( 'organizations.list.email') }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                        <div class="form-group">
                            <label>{{ __('organizations.list.password') }}</label>
                            <input class="form-control" type="password" name="password"
                                   placeholder="{{ __('organizations.list.password') }}">
                        </div>
                    </div>
                    <div class="col-auto col-sm-auto col-md-auto col-lg-6 col-xl-6">
                        <div class="form-group">
                            <label>{{ __('organizations.list.password_confirm') }}</label>
                            <input class="form-control" type="password"
                                   name="password_confirmation"
                                   placeholder="{{ __('organizations.list.password_confirm') }}">
                        </div>
                    </div>
                </div>
            </div>
            @include('crm.general_edit_footer')
        </form>
    </div>
@stop
