@extends('crm.general')

@section('content_general')
    <div class="card">
        <form method="POST" enctype="multipart/form-data"
              data-attr-lang="yes"
              role="form" action="{{ $route }}" id="form"
              onsubmit="Main.sendFormData(event, $(this))">
            @if(!empty($model))
                {{ method_field('PATCH') }}
            @endif
            @csrf
            <div class="row">
                <div class="col-auto col-sm-auto col-md-auto col-lg-8 col-xl-8">
                    <div class="card-header">
                        <ul class="nav nav-tabs" id="translationTab" role="tablist">
                            @foreach($languages as $item)
                                <li class="nav-item" id="{{$item->name}}">
                                    <a class="nav-link
                                        @if(!empty($model))
                                    {{$model->language_id == $item->id ? 'active' : ''}}
                                    @else
                                    {{Auth::user()->language_value_id == $item->id ? 'active' : ''}}
                                    @endif"
                                       id="{{ $item->name }}-tab" data-toggle="tab"
                                       href="#content-{{$item->name}}" role="tab" aria-controls="home"
                                       aria-selected="true">
                                        <span><img src="{{asset($item->icon)}}"
                                                   style="width: 32px;"> {{$item->name}}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="translationTabContent">
                            @foreach($languages as $item)
                                {{ App::setLocale($item->name) }}
                                <div class="tab-pane fade show
                            @if(!empty($model))
                                {{$model->language_id == $item->id ? 'active' : ''}}
                                @else
                                {{Auth::user()->language_value_id == $item->id ? 'active' : ''}}
                                @endif"
                                     id="content-{{$item->name}}" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="row">
                                        <div class="col-auto col-sm-auto col-md-auto col-lg-4 col-xl-4">
                                            <div class="form-group">
                                                <label>{{ __($translation . 'list.surname') }}</label>
                                                <input name="{{$item->name}}[surname]"
                                                       placeholder="{{ __($translation . 'list.surname') }}"
                                                       @if(!empty($model))
                                                       value="{{$model->paramOutput($item, 'surname')}}"
                                                       @else
                                                       value="{{ old($item->name.'surname') }}"
                                                       @endif
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-auto col-sm-auto col-md-auto col-lg-4 col-xl-4">
                                            <div class="form-group">
                                                <label>{{ __($translation . 'list.first_name') }}</label>
                                                <input name="{{$item->name}}[first_name]"
                                                       placeholder="{{ __($translation . 'list.first_name') }}"
                                                       @if(!empty($model))
                                                       value="{{$model->paramOutput($item, 'first_name')}}"
                                                       @else
                                                       value="{{ old($item->name.'first_name') }}"
                                                       @endif
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-auto col-sm-auto col-md-auto col-lg-4 col-xl-4">
                                            <div class="form-group">
                                                <label>{{ __($translation . 'list.middle_name') }}</label>
                                                <input name="{{$item->name}}[middle_name]"
                                                       placeholder="{{ __($translation . 'list.middle_name') }}"
                                                       @if(!empty($model))
                                                       value="{{$model->paramOutput($item, 'middle_name')}}"
                                                       @else
                                                       value="{{ old($item->name.'middle_name') }}"
                                                       @endif
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __($translation . 'list.address') }}</label>
                                        <input type="text" class="form-control"
                                               name="{{$item->name}}[address]"
                                               @if(!empty($model))
                                               value="{{$model->paramOutput($item, 'address')}}"
                                               @else
                                               value="{{ old($item->name.'address') }}"
                                            @endif
                                        >
                                    </div>
                                    <div class="form-group">
                                        <label>{{ __($translation . 'list.about_me') }}</label>
                                        <textarea name="{{$item->name}}[about_me]" class="form-control" cols="30"
                                                  rows="3" placeholder="{{ __($translation . 'list.about_me') }}"
                                        >@if(!empty($model)){!! $model->paramOutput($item, 'about_me') !!}@else{!! old($item->name.'about_me')  !!}@endif</textarea>
                                    </div>
                                </div>
                            @endforeach
                            {{ App::setlocale(Auth::user()->languageValue->name) }}
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-auto col-sm-auto col-md-auto col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>{{ __('organizations.list.password') }}</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>
                            <div class="col-auto col-sm-auto col-md-auto col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>{{ __('organizations.list.password_confirm') }}</label>
                                    <input type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-auto col-sm-auto col-md-auto col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>{{ __($translation . 'list.email') }}</label>
                                    <input type="email" class="form-control" name="email"
                                           value="{{ !empty($model) ? $model->email : old('email') }}">
                                </div>
                            </div>
                            <div class="col-auto col-sm-auto col-md-auto col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>{{ __($translation . 'list.date_of_birth') }}</label>
                                    <input type="text" class="form-control" data-date="true" id="date_of_birth"
                                           name="date_of_birth" data-language="{{Auth::user()->languageValue->name}}"
                                           value="{{ !empty($model) ? $model->date_of_birth : old('date_of_birth') }}"
                                    >
                                </div>
                            </div>
                            <div class="col-auto col-sm-auto col-md-auto col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>{{ __($translation . 'list.start_date_job') }}</label>
                                    <input type="text" class="form-control" data-date="true" id="start_date_job"
                                           name="start_date_job" data-language="{{Auth::user()->languageValue->name}}"
                                           value="{{ !empty($model) ? $model->start_date_job : old('start_date_job') }}"
                                    >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @if((!empty($model) && $model->id != Auth::user()->id) || empty($model))
                                <div class="col-auto col-sm-auto col-md-auto col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label>{{ __($translation . 'list.status') }}</label>
                                        <select name="status_user_id" class="form-control select2"
                                                data-language="{{ Auth::user()->languageValue->name }}"
                                                data-placeholder="{{ __($translation . 'list.status') }}">
                                            <option></option>
                                            @foreach($statusUsers as $status)
                                                <option value="{{$status->id}}"
                                                        @if(!empty($model) && $status->id == $model->status_user_id) selected @endif
                                                >{{ $status->paramTableOutput('name') }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="col-auto col-sm-auto col-md-auto col-lg-4 col-xl-4">
                                <div class="form-group">
                                    <label>{{ __($translation . 'list.phone') }}</label>
                                    <input type="text" class="form-control phone"
                                           name="phone"
                                           value="{{ !empty($model) ? $model->phone : old('phone') }}"
                                    >
                                </div>
                            </div>
                            @if((!empty($model) && $model->id != Auth::user()->id) || empty($model))
                                <div class="col-auto col-sm-auto col-md-auto col-lg-4 col-xl-4">
                                    <div class="form-group">
                                        <label>{{ __($translation . 'list.roles') }}</label>
                                        <select name="role_id" class="form-control select2"
                                                data-language="{{ Auth::user()->languageValue->name }}"
                                                data-placeholder="{{ __($translation . 'list.roles') }}">
                                            <option></option>
                                            @foreach($roles as $role)
                                                <option value="{{$role->id}}"
                                                        @if(!empty($model) && $role->id == $model->roles()->first()->id) selected @endif
                                                >{{ $role->paramTableOutput('display_name') }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-auto col-sm-auto col-md-auto col-lg-4 col-xl-4">
                    <div class="card-body">
                        <div class="form-group  justify-content-center d-flex">
                            <img src="{{ !empty($model) ? $model->adminlte_image() : asset('/img/stub.png') }}"
                                 style="width: 183px;height: 183px;"
                                 class="user-image img-circle elevation-2" data-logo="true"
                                 alt="{{!empty($model) ? $model->full_name : ''}}">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-block btn-primary" type="button"
                                    onclick="$('#file').click()">{{ __('general.button.photo') }}</button>
                            <input type="file" class="d-none" name="photo" id="file">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                @include('crm.general_edit_footer')
            </div>
        </form>
        @stop
        @section('js')
            <script>
                $("#file").change(function () {
                    Main.readURL(this);
                });
            </script>
@stop
