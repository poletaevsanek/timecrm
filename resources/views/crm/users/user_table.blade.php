<div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <img src="{{ $query->adminlte_image() }}" class="user-image img-circle elevation-2" alt="{{$query->fio}}">
    <div class="info">
        {{$query->paramTableOutput('full_name', Auth::user()->language_value_id)}}
    </div>
</div>
