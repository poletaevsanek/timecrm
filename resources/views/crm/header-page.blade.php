<div class="row">
    @if(isset($routeCreate))
        <div class="col-auto col-sm-auto col-md-auto col-lg-3 col-xl-3">
            @include('crm.addButton')
        </div>
    @endif
    <div class="col-auto col-sm-auto col-md-auto col-lg-4 col-xl-4">
        <input type="text"  class="form-control search-input" data-type="table_search"
               placeholder="{{__('general.search')}}">
    </div>
</div>
