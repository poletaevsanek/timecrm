<button type="button"
        onclick="Main.deleteMethodLE('{{ route($routeName . 'destroy', $element->id) }}', '{{ $element->name ?? $element->display_name ?? $element->title ?? '' }}');"
        class="btn btn-xs btn-danger
         {{-- fa-pull-right --}}
         " style="width: 27px; margin-right: 5px;"
        title="{{ __('general.delete') }}"><i class="fas fa-times" style="font-size: 17px;"></i></button>

