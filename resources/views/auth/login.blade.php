@extends('adminlte::auth.login')
@section('auth_footer_language')
    @if(isset($languages) && !$languages->isEmpty())
        <hr/>
        <div class="my-0 text-center">
            @foreach($languages as $language)
                <a href="{{route('language.edit', $language)}}"
                   class="{{ Session::get('language') ==  $language->id  ? 'btn btn-primary' : ''}}">
                    @include('crm.language.menu-item-language')
                </a>
            @endforeach
        </div>
    @endif
@stop
