<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['language']], function () {
    Route::resource('language', 'System\LanguageController')->only('edit');

    Auth::routes();

    Route::group(['middleware' => ['auth']], function () {
        /**Организации*/
        Route::resource('organizations', 'Organization\OrganizationController');
        Route::resource('system-settings', 'Settings\SystemSettingController');

        Route::resource('branches', 'Organization\BranchController');
        Route::resource('roles', 'Settings\RoleController');
        Route::get('edit-branches/{id}', 'Organization\BranchController@editBrach')->name('branches.editBranch');

        Route::group(['middleware' => ['branches']], function () {

            Route::get('/', function () {
                return view('home');
            });
            /**Роли*/
            Route::resource('users', 'Users\UserController');
        });

    });
});
