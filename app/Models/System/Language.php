<?php

namespace App\Models\System;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends BaseModel
{
    use HasFactory;

    public function roles()
    {
        return $this->hasMany(\App\Models\RoleSystem::class);
    }

    public function statusUsers()
    {
        return $this->hasMany(StatusUser::class);
    }
}
