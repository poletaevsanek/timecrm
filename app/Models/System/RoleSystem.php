<?php

namespace App\Models\System;

use Spatie\Permission\Models\Role;

class RoleSystem extends Role
{
    public function roleLanguage()
    {
        return $this->hasMany(RoleLanguage::class);
    }

    public function paramOutput($lang, $attr)
    {
        if($this->language_id == $lang->id)
            return $this->$attr;
        else {

            if(!empty($this->roleLanguage->where('language_id', $lang->id)))
                return  $this->translations->where('language_id', $lang->id)->first()->name;
        }
        return '';
    }
}
