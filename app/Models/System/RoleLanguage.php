<?php

namespace App\Models\System;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleLanguage extends BaseModel
{
    use HasFactory;

    protected $fillable = ['role_id', 'name', 'language_id'];
}
