<?php

namespace App\Models\Settings;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Image;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class SystemSetting extends BaseModel  implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'system_name',
        'logo',
        'background_logo',
        'language_id',
    ];

    public function addLogoPhoto($photo)
    {
        Image::load($photo)->fit('max', 145, 145)->save();
        return $this->addMedia($photo)->preservingOriginal()->toMediaCollection('logo');
    }

    public function deleteLogoPhoto()
    {
        return $this->getMedia('logo')->last()->delete();
    }

    public function addBackgroundPhoto($photo)
    {
        Image::load($photo)->fit('max', 1920, 1080)->save();
        return $this->addMedia($photo)->preservingOriginal()->toMediaCollection('background_logo');
    }

    public function deleteBackgroundPhoto()
    {
        return $this->getMedia('background_logo')->last()->delete();
    }

    public function logo_photo()
    {
        if ($this->logo) {
            return $this->getMedia('logo')->last()->getUrl();
        } else {
            return asset('/img/AdminLTELogo.png');
        }
    }

    public function bg_photo()
    {
        if($this->background_logo) {
            return $this->getMedia('background_logo')->last()->getUrl();
        } else {
            return asset('/img/background.jpg');
        }
    }
}
