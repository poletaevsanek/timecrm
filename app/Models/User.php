<?php

namespace App\Models;

use App\Models\Organization\Branch;
use App\Models\Organization\Organization;
use App\Models\System\Language;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable  implements HasMedia
{
    use HasFactory, Notifiable, HasRoles, SoftDeletes, InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'surname', 'first_name', 'middle_name',
        'email',
        'date_of_birth',
        'start_date_job',
        'password',
        'avatar',
        'address', 'about_me',
        'status_user_id',
        'organization_id',
        'branch_id',
        'language_id',
        'language_value_id',
        'parent_id',
        'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setDateOfBirthAttribute($date)
    {
        if (!empty($date)) {
            $this->attributes['date_of_birth'] = Carbon::parse($date);
        } else {
            $this->attributes['date_of_birth'] = null;
        }
    }

    public function setStartDateJobAttribute($date)
    {
        if (!empty($date)) {
            $this->attributes['start_date_job'] = Carbon::parse($date);
        } else {
            $this->attributes['start_date_job'] = null;
        }
    }

    public function getDateOfBirthAttribute($date)
    {
        if (!empty($date)) {
            return Carbon::parse($date)->format('d.m.Y');
        }
        return null;
    }

    public function getStartDateJobAttribute($date)
    {
        if (!empty($date)) {
            return Carbon::parse($date)->format('d.m.Y');
        }
        return null;
    }

    public function languageValue()
    {
        return $this->belongsTo(Language::class, 'language_value_id')->withDefault();
    }

    public function translations()
    {
        return $this->hasMany($this, 'parent_id');
    }

    public function translation()
    {
        return $this->belongsTo($this, 'id', 'parent_id');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class)->withDefault();
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class)->withDefault();
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('d.m.Y');
    }

    public function getFullNameAttribute()
    {
        return $this->surname . ' ' . $this->first_name . ' ' . $this->middle_name;
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function paramOutput($lang, $attr)
    {
        if ($this->language_id == $lang->id)
            return $this->$attr;
        else {

            if (!empty($this->translations->where('language_id', $lang->id)))
                return $this->translations->where('language_id', $lang->id)->first()->$attr;
        }
        return '';
    }

    public function paramTableOutput($attr, $lang)
    {
        return $this->translation()->where('language_id', $lang)->first()->$attr ??
            $this->$attr;
    }

    public function adminlte_image()
    {
        if ($this->avatar) {
            return $this->getMedia('avatar')->last()->getUrl();
        } else {
            return '/img/stub.png';
        }
    }

    public function adminlte_profile_url()
    {
        return route('users.edit', $this->id);
    }

    public function company()
    {
        if ($this->organization->branch)
            return $this->branch;
        return $this->organization;
    }
}
