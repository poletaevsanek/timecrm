<?php

namespace App\Models\Organization;

use App\Models\BaseModel;
use App\Models\System\Timezone;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends CompanyModel
{
    use HasFactory;

    protected $fillable = ['name', 'organization_id', 'language_id', 'timezone_id', 'parent_id'];
}
