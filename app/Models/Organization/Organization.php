<?php

namespace App\Models\Organization;

use App\Models\BaseModel;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Organization extends CompanyModel implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = ['name', 'branch', 'language_id', 'timezone_id', 'parent_id'];

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('d.m.Y');
    }

    public function logo_image()
    {
        if($this->logo) {
            return $this->getMedia('logo')->last()->getUrl();
        } else {
            return '/img/stub.png';
        }
    }


    public function branches()
    {
        return $this->hasMany(Branch::class);
    }
}
