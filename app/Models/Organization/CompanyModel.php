<?php

namespace App\Models\Organization;

use App\Models\BaseModel;
use App\Models\RoleSystem;
use App\Models\System\Timezone;
use App\Models\User;

class CompanyModel extends BaseModel
{
    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function timezone()
    {
        return $this->belongsTo(Timezone::class)->withDefault();
    }

    public function roles()
    {
        return $this->hasMany(RoleSystem::class);
    }
}
