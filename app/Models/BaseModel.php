<?php

namespace App\Models;

use App\Models\System\Language;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BaseModel  extends Model
{
    use SoftDeletes;

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function translations()
    {
        return $this->hasMany($this, 'parent_id');
    }

    public function translation()
    {
        return $this->belongsTo($this, 'id','parent_id');
    }

    public function paramOutput($lang, $attr)
    {
        if($this->language_id == $lang->id)
            return $this->$attr;
        else {

            if(!empty($this->translations->where('language_id', $lang->id)))
                return  $this->translations->where('language_id', $lang->id)->first()->$attr;
        }
        return '';
    }

    public function paramTableOutput($attr)
    {
        $language = Auth::check() ? Auth::user()->language_value_id : Session::get('language');
        if(empty($language))
            $language = Language::first();
        else
            $language = Language::find($language);
        return $this->translation()->where('language_id', $language->id)->first()->$attr ??
        $this->$attr;
    }
}
