<?php

namespace App\Models;

use App\Models\System\Language;
use App\Models\System\RoleLanguage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Role;

class RoleSystem extends Role
{
    use HasFactory;

    protected $fillable = ['name', 'display_name', 'organization_id'];

    public function language()
    {
        return $this->belongsTo(Language::class)->withDefault();
    }

    public function translations()
    {
        return $this->hasMany(RoleLanguage::class, 'role_id');
    }

    public function paramOutput($lang, $attr)
    {
        if($this->language_id == $lang->id)
            return $this->$attr;
        else {

            if(!empty($this->translations->where('language_id', $lang->id)))
                return  $this->translations->where('language_id', $lang->id)->first()->name;
        }
        return '';
    }

    public function paramTableOutput($attr)
    {
        $language = Auth::check() ? Auth::user()->language_value_id : Session::get('language');
        if(empty($language))
            $language = Language::first();
        else
            $language = Language::find($language);
        return $this->translations()->where('language_id', $language->id)->first()->name ??
            $this->$attr;
    }
}
