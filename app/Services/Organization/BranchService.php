<?php


namespace App\Services\Organization;

use App\Models\Organization\Branch;
use App\Models\System\Language;
use App\Models\System\Timezone;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;

class BranchService extends BaseService
{
    public $templatePath = 'crm.branches.';
    public $translation = 'branches.';
    public $routeName = 'branches.';
    public $model;

    public function __construct()
    {
        parent::__construct(Branch::class);
    }

    public function tableColumns()
    {
        $columns = [
            [
                'title' => __('general.list.id'),
                'data' => 'id',
                'width' => '5%'
            ],
            [
                'title' => __('general.list.name'),
                'data' => 'name',
            ],
            [
                'title' => __($this->translation . 'list.timezone'),
                'data' => 'timezone',
                'name' => 'timezone_id',
            ]
        ];
        if(Auth::user()->hasPermissionTo('branches-edit') || Auth::user()->hasPermissionTo('branches-delete'))
            $columns[] = $this->actionButton();
        return $columns;
    }

    public function outputData()
    {
        $with = $this->baseOutputData(Auth::user()->hasPermissionTo('branches-edit'));
        return $with;
    }

    public function editElement($branch = null)
    {
        $with = $this->baseEditElement($branch);
        $with += [
            'timezones' => Timezone::all()
        ];
        return $with;
    }

    public function getDatatableElements()
    {
        $query = Auth::user()->organization->branches()->whereNull('parent_id');
        return $this->dataTableData($query, ['translation'])
            ->editColumn('name', function ($query){
                return $query->paramTableOutput('name');
            })
            ->editColumn('timezone', function ($query){
                return $query->timezone->name.'/'.$query->timezone->timezone;
            })

            ->rawColumns(['action', 'logo'])
            ->make(true);
    }

    public function storeElement($request)
    {
        $requestAll = $request->all();
        unset($requestAll['_token']);
        $language = Auth::user()->languageValue;
        $requestAll[$language->name]['language_id'] = $language->id;
        $requestAll[$language->name]['timezone_id'] = $requestAll['timezone_id'];
        $branch = Auth::user()->organization->branches()->create($requestAll[$language->name]);
        foreach ($requestAll as $key => $item) {
            $itemLanguage = Language::where('name', $key)->first();
            if (!empty($item['name']) && $key != $language->name && !empty($itemLanguage)) {
                $item['timezone_id'] = $requestAll['timezone_id'];
                $item['organization_id'] = $branch->organization_id;
                $item['language_id'] = $itemLanguage->id;
                $this->storeBase($item, $branch);
            }
        }
        Auth::user()->branch_id = $branch->id;
        Auth::user()->save();
        return [
            'name' => __($this->translation . 'title') . ':' . $branch->name,
            'route' => route($this->routeName . 'index')
        ];
    }

    public function updateElement($model, $request)
    {
        $requestAll = $request->all();
        unset($requestAll['_token']);
        unset($requestAll['_method']);
        foreach ($requestAll as $key => $item) {
            $itemLanguage = Language::where('name', $key)->first();
            if (!empty($item['name'])  && !empty($itemLanguage)) {
                $item['timezone_id'] = $requestAll['timezone_id'];
                $this->editBase($item, $model, $itemLanguage);
            }
        }
        return [
            'name' => __($this->translation . 'title') . ':' . $model->name,
            'route' => route($this->routeName . 'index')
        ];
    }

    public function editTitle($model)
    {
        return __($this->translation . 'edit', ['attribute' => $model->name . ' №'.$model->id]);
    }
}
