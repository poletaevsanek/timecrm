<?php

namespace App\Services\Organization;

use App\Models\Organization\Organization;
use App\Models\System\Language;
use App\Models\System\Timezone;
use App\Models\User;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class OrganizationService extends BaseService
{
    public $templatePath = 'crm.organizations.';
    public $translation = 'organizations.';
    public $routeName = 'organizations.';
    public $model;

    public function __construct()
    {
        parent::__construct(Organization::class);
    }

    public function tableColumns()
    {
        $columns = [
            [
                'title' => __('general.list.id'),
                'data' => 'id',
                'width' => '5%'
            ],
            [
                'title' => __($this->translation . 'list.logo'),
                'data' => 'logo',
                'width' => '10%',
            ],
            [
                'title' => __('general.list.name'),
                'data' => 'name',
            ],
            [
                'title' => __($this->translation . 'list.count'),
                'data' => 'count',
                'remove_select' => true,
            ],
            [
                'title' => __($this->translation . 'list.branch'),
                'data' => 'branch',
            ],
            [
                'title' => __($this->translation . 'list.date'),
                'data' => 'created_at',
            ],
        ];
        if(Auth::user()->hasPermissionTo('organizations-edit') || Auth::user()->hasPermissionTo('organizations-delete'))
            $columns[] = $this->actionButton();
        return $columns;
    }

    public function outputData()
    {
        $with = $this->baseOutputData(Auth::user()->hasAnyRole(['superadmin']));
        return $with;
    }

    public function getDatatableElements()
    {
        $query = $this->model::whereNull('parent_id');
        return $this->dataTableData($query, ['translation'])
            ->editColumn('logo', function ($query) {
                return view($this->templatePath . 'image_table', compact('query'));
            })
            ->editColumn('name', function ($query){
                return $query->paramTableOutput('name');
            })
            ->editColumn('count', function ($query){
                return $query->users()->whereNull('parent_id')->count();
            })
            ->editColumn('branch', function ($query){
                if($query->branch)
                    return $query->branches()->whereNull('parent_id')->count();
                return '-';
            })
            ->rawColumns(['action', 'logo'])
            ->make(true);
    }

    public function editElement($organization = null)
    {
        $with = $this->baseEditElement($organization);
        $with += [
            'timezones' => Timezone::all()
        ];
        return $with;
    }

    public function storeElement($request)
    {
        $requestAll = $request->all();
        unset($requestAll['_token']);
        $language = Auth::user()->languageValue;
        if (isset($requestAll['branch']))
            $requestAll[$language->name]['branch'] = 1;
        else
            $requestAll[$language->name]['branch'] = 0;
        $requestAll[$language->name]['language_id'] = $language->id;
        $requestAll[$language->name]['timezone_id'] = $requestAll['timezone_id'];
        $organization = $this->model::create($requestAll[$language->name]);
        if (isset($requestAll['logo']) && !empty($requestAll['logo'])) {
            $this->addFile($organization, $requestAll['logo'], 'img', 'logo');
            $organization->logo = 1;
            $organization->save();
        }
        unset($requestAll['logo']);
        foreach ($requestAll as $key => $item) {
            $itemLanguage = Language::where('name', $key)->first();
            if (!empty($item['name']) && $key != $language->name && !empty($itemLanguage)) {
                $item['language_id'] = $itemLanguage->id;
                $item['timezone_id'] = $requestAll['timezone_id'];
                $this->storeBase($item, $organization);
            }
        }



        $this->storeUser($requestAll, $organization, $language);

        return [
            'name' => __($this->translation . 'title') . ':' . $organization->name,
            'route' => route($this->routeName . 'index')
        ];
    }

    public function storeUser($requestAll, $organization, $language)
    {
        $data = [
            'surname' => $requestAll[$language->name]['surname'],
            'first_name' => $requestAll[$language->name]['first_name'],
            'email' => $requestAll[$language->name]['email'],
            'password' => Hash::make($requestAll[$language->name]['password']),
            'status_user_id' => 1,
            'organization_id' => $organization->id,
            'language_id' => $language->id,
            'language_value_id' => $language->id,
        ];
        $user = User::create($data);
        $user->assignRole('admin_organization');
        foreach ($requestAll as $key => $item) {
            $itemLanguage = Language::where('name', $key)->first();
            if (!empty($item) && $key != $language->name && !empty($itemLanguage)) {
                $data['language_id'] = $itemLanguage->id;
                $data['surname'] = !empty($item['surname']) ? $item['surname'] : $data['surname'];
                $data['first_name'] = !empty($item['first_name']) ? $item['first_name'] : $data['first_name'];
                $data['email'] = $requestAll[$language->name]['email'] . '_' . $itemLanguage->name;
                $this->storeBase($data, $user);
            }
        }
        return true;
    }

    public function updateElement($model, $request)
    {
        $requestAll = $request->all();
        unset($requestAll['_token']);
        unset($requestAll['_method']);
        if (isset($requestAll['logo']) && !empty($requestAll['logo'])) {
            $this->addFile($model, $requestAll['logo'], 'img', 'logo');
            $model->logo = 1;
            $model->save();
        }
        unset($requestAll['logo']);
        foreach ($requestAll as $key => $item) {
            $itemLanguage = Language::where('name', $key)->first();
            if (!empty($item['name'])  && !empty($itemLanguage)) {
                $item['timezone_id'] = $requestAll['timezone_id'];
                $this->editBase($item, $model, $itemLanguage);
            }
        }
        return [
            'name' => __($this->translation . 'title') . ':' . $model->name,
            'route' => route($this->routeName . 'index')
        ];
    }

    public function destroyElement($id)
    {
        $model = $this->model::find($id);
        $model->users()->delete();
        $model->translations()->delete();
        $model->delete();
        return true;
    }

    public function editTitle($model)
    {
        return __($this->translation . 'edit', ['attribute' => $model->name . ' №'.$model->id]);
    }
}
