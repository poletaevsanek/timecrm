<?php


namespace App\Services;


use App\Models\System\Language;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LanguageService
{
    public function editLanguage($language)
    {
        $language = Language::find($language);

        if(empty($language))
            $language  = Language::first();

        Session::put('language', $language->id);
        if(Auth::check()){
            Auth::user()->language_value_id = $language->id;
            Auth::user()->save();
        }
        return true;
    }
}
