<?php


namespace App\Services\Users;

use App\Models\RoleSystem;
use App\Models\System\Language;
use App\Models\System\StatusUser;
use App\Models\System\Timezone;
use App\Models\User;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserService extends BaseService
{
    public $templatePath = 'crm.users.';
    public $translation = 'users.';
    public $routeName = 'users.';
    public $model;

    public function __construct()
    {
        parent::__construct(User::class);
    }

    public function tableColumns()
    {
        $columns = [
            [
                'title' => __('general.list.id'),
                'data' => 'id',
                'name' => 'users.id',
                'width' => '5%'
            ],
            [
                'title' => __($this->translation . 'list.fio'),
                'data' => 'full_name',
                'name' => DB::raw("users.surname, users.first_name, users.middle_name, users.language_id, users.avatar"),
            ],
        ];
        if (Auth::user()->hasAnyRole(['superadmin'])) {
            $columns[] =
                [
                    'title' => __($this->translation . 'list.organization'),
                    'data' => 'organization_id',
                ];
            $columns[] = [
                'title' => __('organizations.list.date'),
                'data' => 'created_at',
            ];
            $columns[] = [
                'title' => __('organizations.list.email'),
                'data' => 'email',
            ];

        } else {
            $columns[] = [
                'title' => __($this->translation . 'list.roles'),
                'data' => 'roles',
                'remove_select' => true,
            ];
            $columns[] = [
                'title' => __('organizations.list.email'),
                'data' => 'email',
            ];
            $columns[] = [
                'title' => __($this->translation . 'list.phone'),
                'data' => 'phone',
            ];
            $columns[] = [
                'title' => __( $this->translation . 'list.date_of_birth'),
                'data' => 'date_of_birth',
            ];
            $columns[] = [
                'title' => __($this->translation . 'list.start_date_job'),
                'data' => 'start_date_job',
            ];
        }
        if (Auth::user()->hasPermissionTo('users-edit') || Auth::user()->hasPermissionTo('users-delete'))
            $columns[] = $this->actionButton();

        return $columns;
    }

    public function outputData()
    {
        $with = $this->baseOutputData(!Auth::user()->hasAnyRole(['superadmin']) && Auth::user()->hasPermissionTo('users-edit') ?? false);
        return $with;
    }

    public function getDatatableElements()
    {
        if (Auth::user()->hasAnyRole(['superadmin']))
            $query = $this->model::whereHas('roles', function ($query) {
                return $query->whereName('admin_organization');
            });
        else
            $query = Auth::user()->company()->users();
        $query = $query->whereNull('parent_id');
        return $this->dataTableData($query, ['organization'])
            ->editColumn('full_name', function ($query){
                 return view($this->templatePath . 'user_table')->with(['query' => $query]);
            })
            ->editColumn('roles', function ($query){
                $role = $query->roles()->first();
                return RoleSystem::find($role->id)->paramTableOutput('display_name');
            })
            ->editColumn('organization_id', function ($query){
                return $query->organization->paramTableOutput('name');
            })
            ->make(true);
    }

    public function editElement($organization = null)
    {
        if(Auth::user()->hasAnyRole(['superadmin'])){
            $this->templatePath = 'crm.users.user_organization.';
        }
        $with = $this->baseEditElement($organization);
        $with += [
            'statusUsers' => StatusUser::whereNull('parent_id')->get(),
            'roles' => Auth::user()->company()->roles,
        ];
        return $with;
    }

    public function storeElement($request)
    {
        $requestAll = $request->all();
        unset($requestAll['_token']);
        $language = Auth::user()->languageValue;
        $data = [
            'surname' => $requestAll[$language->name]['surname'],
            'first_name' => $requestAll[$language->name]['first_name'],
            'middle_name'  => $requestAll[$language->name]['middle_name'],
            'email' => $requestAll['email'],
            'date_of_birth' => $requestAll['date_of_birth'],
            'start_date_job' => $requestAll['start_date_job'],
            'password' => Hash::make($requestAll['password']),
            'address' => $requestAll[$language->name]['about_me'],
            'about_me' => $requestAll[$language->name]['about_me'],
            'status_user_id' => $requestAll['status_user_id'],
            'organization_id' => Auth::user()->organization_id,
            'branch_id'  => Auth::user()->branch_id,
            'language_id' => $language->id,
            'language_value_id' => $language->id,
            'phone' => $requestAll['phone'],
        ];
        $user = $this->model::create($data);
        $role = Role::find($requestAll['role_id'])->name;
        $user->assignRole($role);
        if (isset($requestAll['photo']) && !empty($requestAll['photo'])) {
            $this->addFile($user, $requestAll['photo'], 'image', 'avatar');
            $user->avatar = 1;
            $user->save();
        }
        unset($requestAll['photo']);
        foreach ($requestAll as $key => $item) {

            $itemLanguage = Language::where('name', $key)->first();
            if ((isset($item['surname']) || isset($item['first_name']))  && !empty($itemLanguage) && $language->id != $itemLanguage->id) {
                $data['language_id'] = $itemLanguage->id;
                $data['surname'] = !empty($item['surname']) ? $item['surname'] : $data['surname'];
                $data['first_name'] = !empty($item['first_name']) ? $item['first_name'] : $data['first_name'];
                $data['middle_name'] = !empty($item['middle_name']) ? $item['middle_name'] : $data['middle_name'];
                $data['address'] = !empty($item['address']) ? $item['address'] : $data['address'];
                $data['about_me'] = !empty($item['about_me']) ? $item['about_me'] : $data['about_me'];
                $data['email'] = $requestAll['email'] . '_' . $itemLanguage->name;
                $this->storeBase($data, $user);
            }
        }

        return [
            'name' => __($this->translation . 'title') . ':' . $user->paramTableOutput('full_name', Auth::user()->language_value_id),
            'route' => route($this->routeName . 'index')
        ];
    }

    public function updateElement($model, $request)
    {
        $requestAll = $request->all();
        $data = [];
        if(!empty($requestAll['password'])){
            $requestAll['password'] = Hash::make($requestAll['password']);
            $data['password'] = Hash::make($requestAll['password']);
        } else {
            unset($requestAll['password']);
        }
        if(Auth::user()->hasAnyRole(['superadmin'])){
            $model->update($requestAll);
        } else {
            $language = $model->language;
            $data += [
                'surname' => $requestAll[$language->name]['surname'],
                'first_name' => $requestAll[$language->name]['first_name'],
                'middle_name'  => $requestAll[$language->name]['middle_name'],
                'email' => $requestAll['email'],
                'date_of_birth' => $requestAll['date_of_birth'],
                'start_date_job' => $requestAll['start_date_job'],
                'address' => $requestAll[$language->name]['about_me'],
                'about_me' => $requestAll[$language->name]['about_me'],
                'phone' => $requestAll['phone'],
            ];
            if(isset($requestAll['status_user_id'])){
                $data['status_user_id'] = $requestAll['status_user_id'];
            }
            if(isset($requestAll['role_id'])) {
                $role = Role::find($requestAll['role_id'])->name;
                $model->assignRole($role);
            }
            unset($requestAll['_token']);
            unset($requestAll['_method']);
            if (isset($requestAll['photo']) && !empty($requestAll['photo'])) {
                $this->addFile($model, $requestAll['photo'], 'image', 'avatar');
                $model->avatar = 1;
                $model->save();
            }
            unset($requestAll['photo']);
            foreach ($requestAll as $key => $item) {
                $itemLanguage = Language::where('name', $key)->first();
                if ((isset($item['surname']) || isset($item['first_name']))  && !empty($itemLanguage)) {
                    $data['surname'] = !empty($item['surname']) ? $item['surname'] : $data['surname'];
                    $data['first_name'] = !empty($item['first_name']) ? $item['first_name'] : $data['first_name'];
                    $data['middle_name'] = !empty($item['middle_name']) ? $item['middle_name'] : $data['middle_name'];
                    $data['address'] = !empty($item['address']) ? $item['address'] : $data['address'];
                    $data['about_me'] = !empty($item['about_me']) ? $item['about_me'] : $data['about_me'];
                    if($itemLanguage->id != $language->id)
                        $data['email'] = $requestAll['email'] . '_' . $itemLanguage->name;
                    $this->editBase($data, $model, $itemLanguage);
                }
            }
        }
        return [
            'name' => __($this->translation . 'title') . ':' . $model->paramTableOutput('full_name', Auth::user()->language_value_id),
            'route' => route($this->routeName . 'index')
        ];
    }

    public function editTitle($model)
    {
        return __($this->translation . 'edit', ['attribute' => $model->full_name . ' №'.$model->id]);
    }
}
