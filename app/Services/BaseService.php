<?php

namespace App\Services;
use Spatie\Image\Image;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder as BuilderDT;

class BaseService
{
    public $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function baseOutputData($route)
    {
        $with['title'] = __($this->translation . 'title');
        $with['datatable'] = $this->constructViewDT();
        $newModel = new $this->model;
        $with['permission'] = $newModel->getTable();
        $with['translation'] = $this->translation;
        if($route)
            $with['routeCreate'] = route($this->routeName . 'create');
        return $with;
    }

    public function baseEditElement($model)
    {
        $with = [
            'title' => empty($model) ? __($this->translation . 'create') : $this->editTitle($model),
            'translation' => $this->translation,
            'routeBack' => route($this->routeName . 'index'),
            'route' => empty($model) ? route($this->routeName . 'store') : route($this->routeName . 'update', $model),
            'model' => $model
        ];
        return $with;
    }

    public function editTitle($model)
    {
        return __($this->translation . 'edit');
    }

    public function constructViewDT($selectorForm = '#dt_filters')
    {
        return app(BuilderDT::class)
            ->language(config('datatables.lang'))
            ->orders([0, 'desc'])
            ->pageLength(50)
            ->ajaxWithForm('', $selectorForm)
            ->columns( $this->tableColumns())
            ->parameters([
                'paging' => true,
                'searching' => true,
                'sDom' => '<"top">rt<"bottom"><div>p<"clear">',
                'searchHighlight' => true
            ]);
    }

    public function tableColumns()
    {
        return [
            [
                'title' => __('general.list.id'),
                'data' => 'id',
                'width' => '5%'
            ],
            [
                'title' => __('general.list.name'),
                'data' => 'name',
            ],

            $this->actionButton()
        ];
    }

    /**
     * Формирует данные для шаблона "Список элементов"
     */
    public function dataTableData($query = null, $with = [], $onClick = null)
    {
        $query = !empty($query) ?
            $query->select($this->columnsToSelect( $this->tableColumns()))->with($with) :
            $this->model::select($this->columnsToSelect( $this->tableColumns()))->with($with);

        $data =  Datatables::of($query)
            ->addColumn('action', function ($element) use ($onClick) {
                return view('crm.action_buttons')->with(['element' => $element, 'routeName' => $this->routeName, 'onClick' => $onClick]);
            });

        return $data;
    }

    /**
     * Получаем только заголовки колонок для DT
     */
    protected function columnsToSelect($array)
    {
        if(!empty($array)){
            $resultArray = [];

            foreach($array as $value){
                if(!isset($value['remove_select']) or $value['remove_select'] !== true) {
                    if(isset($value['name'])) {
                        $resultArray[] = $value['name'];
                    } else {
                        $resultArray[] = $value['data'];
                    }
                }
            }

            return $resultArray;
        }

        return $array;
    }

    /**
     * Стандартная кнопка действия для DataTable
     */
    protected function actionButton()
    {
        return [
            'title' => __('general.list.action'),
            'remove_select' => true,
            'width' => '10%',
            'data' => 'action',
            'searchable' => false,
            'orderable' => false,
        ];
    }

    /**
     * Формируем ответ общего стандарта
     */

    public function storeBase($request, $model)
    {
        $model->translations()->create($request);
        return true;
    }

    public function editBase($request, $model, $language)
    {
        if($language->id != $model->language_id)
            $model->translations->where('language_id', $language->id)->first()
                ->update($request);
        else
            $model->update($request);
        return true;
    }

    public function responseSuccess($date = [])
    {
        return response()->json([
            'date' => $date
        ], 200);
    }

    public function responseError($status = 422,$date = [])
    {
        return response()->json([
            'date' => $date
        ], $status);
    }

    /**
     * Загрузка картинок
     */

    public function addFile($model, $file, $type, $mediaCollection='avatar')
    {
        if($type == 'image')
            Image::load($file)->fit('max', 2048, 2048)->save();
        return $model->addMedia($file)->preservingOriginal()->toMediaCollection($mediaCollection);
    }

    public function destroyElement($id)
    {
        $model = $this->model::find($id);
        $model->translations()->delete();
        $model->delete();
        return true;
    }
}
