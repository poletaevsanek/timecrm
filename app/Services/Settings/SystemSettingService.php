<?php


namespace App\Services\Settings;

use App\Models\Settings\SystemSetting;
use App\Models\System\Language;
use App\Services\BaseService;

class SystemSettingService extends BaseService
{
    public $templatePath = 'crm.settings.';
    public $translation = 'settings.';
    public $routeName = 'system-settings.';
    public $model;

    public function __construct()
    {
        parent::__construct(SystemSetting::class);
    }

    public function editElement($model)
    {
        $with = $this->baseEditElement($model);
        return $with;
    }

    public function updateElement($systemSetting, $request)
    {
        $data = $request->all();
        unset($data['_token']);
        unset($data['_method']);
        //$systemSetting->update($data);
        if(isset($data['logo_file'])){
            if ($systemSetting->logo_photo()) {
                $systemSetting->deleteLogoPhoto($data['logo_file']);
            }
            $systemSetting->addLogoPhoto($data['logo_file']);
            $systemSetting->logo = 1;
            $systemSetting->save();
        }
        unset($data['logo_file']);
        if(isset($data['bg_file'])){
            if ($systemSetting->bg_photo()) {
                $systemSetting->deleteBackgroundPhoto($data['bg_file']);
            }
            $systemSetting->addBackgroundPhoto($data['bg_file']);
            $systemSetting->background_logo = 1;
            $systemSetting->save();
        }
        unset($data['bg_file']);
        foreach ($data as $key => $item) {
            $itemLanguage = Language::where('name', $key)->first();
            if (!empty($item['system_name'])  && !empty($itemLanguage)) {
                $this->editBase($item, $systemSetting, $itemLanguage);
            }
        }
        return [
            'name' => __($this->translation . 'edit'),
            'route' => route($this->routeName . 'edit', $systemSetting->id)
        ];
    }

    public function load()
    {
        $systemSetting = SystemSetting::whereNull('parent_id')->get()->first();
        return $systemSetting;

    }
}
