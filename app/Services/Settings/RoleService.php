<?php

namespace App\Services\Settings;

use App\Models\RoleSystem;
use App\Models\System\Language;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;

class RoleService extends BaseService
{
    public $templatePath = 'crm.settings.roles.';
    public $translation = 'roles.';
    public $routeName = 'roles.';
    public $model;

    public function __construct()
    {
        parent::__construct(RoleSystem::class);
    }

    public function outputData()
    {
        return $this->baseOutputData(Auth::user()->hasPermissionTo('roles-edit'));
    }

    public function tableColumns()
    {
        $columns = [
            [
                'title' => __('general.list.id'),
                'data' => 'id',
                'width' => '5%'
            ],
            [
                'title' => __('general.list.name'),
                'data' => 'display_name',
            ]
        ];
        if (Auth::user()->hasPermissionTo('roles-edit') || Auth::user()->hasPermissionTo('roles-delete'))
            $columns[] = $this->actionButton();
        return $columns;
    }

    /**
     * Формирует данные для шаблона "Список элементов"
     */
    public function getDatatableElements()
    {
        $query = Auth::user()->company()->roles();
        return Datatables::of($query)
            ->editColumn('display_name', function($query){
                return $query->paramTableOutput('display_name');
            })
            ->editColumn('action', function ($element) {
                $withoutDestroy = $this->checkEditableRole($element->name) ? false : true;
                return view('crm.action_buttons')->with(['element' => $element, 'routeName' => $this->routeName, 'withoutDestroy' => $withoutDestroy]);
            })
            ->make(true);
    }

    public function editElement($role = null)
    {
        if(!empty($role))
            $role = $this->model::find($role->id);
        $with = $this->baseEditElement($role);
        $with['menu'] = config('adminlte.menu');
        return $with;
    }

    public function editTitle($role)
    {
        return __($this->translation . 'edit', ['attribute' => $role->display_name]);
    }

    private function rewritePermissions(Role $role, array $permissions): bool
    {
        $role->syncPermissions(); //сначала удаляем все пермишоны
        foreach ($permissions as $firstPart => $items) {
            foreach ($items as $secondPart => $item) {
                if ($secondPart != 'parent') {
                    $newPermission = Permission::firstOrCreate(['name' => $firstPart . '-' . $secondPart]);
                } elseif (isset($items['read']) or isset($items['edit']) or isset($items['delete'])) {
                    $newPermission = Permission::firstOrCreate(['name' => $item . '-' . 'read']);
                }
                if (isset($newPermission)) {
                    $role->givePermissionTo($newPermission);
                }
                // если дали пермишон на редактирование или удалене то даем сразу и на чтение
                if ($secondPart === 'edit' or $secondPart === 'delete') {
                    $newPermission = Permission::firstOrCreate(['name' => $firstPart . '-' . 'read']);
                    $role->givePermissionTo($newPermission);
                }
            }
        }

        return true;
    }

    private function checkEditableRole($name): bool
    {
        return !in_array($name, array_keys(config('roles.roles')));
    }

    public function storeElement($request)
    {
        $requestAll = $request->all();
        unset($requestAll['_token']);
        $language = Auth::user()->languageValue;
        $data = ['name' => $requestAll[$language->name]['name'],
            'display_name' => $requestAll[$language->name]['name'],
            'organization_id' => Auth::user()->organization_id,
            'branch_id' => Auth::user()->branch_id,
            'language_id' => $language->id
        ];
        $role = Role::create($data);
        $role = $this->model::find($role->id);
        foreach ($requestAll as $key => $item) {
            $itemLanguage = Language::where('name', $key)->first();
            if (!empty($item['name']) && $key != $language->name && !empty($itemLanguage)) {
                $data['display_name'] = $item['name'];
                $data['name'] = $item['name'];
                $data['language_id'] = $itemLanguage->id;
                $role->translations()->create($data);
            }
        }
        $this->rewritePermissions($role, $request->permissions ?? []);
        return [
            'name' => __($this->translation . 'title'),
            'route' => route($this->routeName . 'index')
        ];
    }

    public function updateElement($role, $request)
    {

        $requestAll =$request->all();
        $role = $this->model::find($role->id);
        $language = $role->language;
        $role->display_name = $requestAll[$language->name]['name'];
        if ($this->checkEditableRole($role->name)) {
            $role->name =$requestAll[$language->name]['name'];
        }
        $role->save();

        foreach ($requestAll as $key => $item) {
            $itemLanguage = Language::where('name', $key)->first();
            if (!empty($item['name'])  && !empty($itemLanguage) && $language->id != $itemLanguage->id) {
                $role->translations->where('language_id', $itemLanguage->id)->first()->update($item);
            }
        }

        $this->rewritePermissions($role, $request->permissions ?? []);
        return [
            'name' => __($this->translation . 'title') . ':' . $role->display_name,
            'route' => route($this->routeName . 'index')
        ];
    }

    public function destroyElement($id): bool
    {
        $role = Role::find($id);
        if ($this->checkEditableRole($role)) {
            $role->delete();
        } else {
            return false;
        }

        return true;
    }
}
