<?php

namespace App\Http\Requests\Settings;

use App\Models\RoleSystem;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class RoleEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(!empty($this->role)) {
            $language = RoleSystem::find($this->role->id)->language->name;
        } else
            $language = Auth::user()->languageValue->name;
        return [
            $language.'.name' => 'required'
        ];
    }

    public function attributes()
    {
        $language = Auth::user()->languageValue->name;
        return [
            $language.'.name' => __('general.list.name')
        ];
    }
}
