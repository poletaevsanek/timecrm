<?php

namespace App\Http\Requests\Settings;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SystemSettingEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $language = $this->system_setting->language;
        return [
            $language->name.'.system_name' => 'required',
            'logo_file' => 'image|nullable',
            'bg_file' => 'image|nullable',
        ];
    }

    public function attributes()
    {
        $language = Auth::user()->languageValue->name;
        return [
            $language.'.system_name' => __('settings.list.name'),
            'logo_file' =>  __('settings.list.logo_file'),
            'bg_file' =>  __('settings.list.bg_file'),
        ];
    }


}
