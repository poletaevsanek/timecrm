<?php

namespace App\Http\Requests\Organizations;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class OrganizationsStorageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $language;
    protected $editRequest;


    public function __construct(OrganizationsEditRequest $organizationsEditRequest)
    {
        $this->language = Auth::user()->languageValue->name;
        $this->editRequest = $organizationsEditRequest;
    }


    public function authorize()
    {
        if(Auth::user()->hasAnyRole(['superadmin']))
            return true;
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $result = $this->editRequest->rules();
        $result += [
            $this->language . '.first_name' => 'required',
            $this->language . '.surname' => 'required',
            $this->language . '.email' => 'required|unique:users,email',
            $this->language . '.password' => 'required|confirmed|min:6',
            $this->language . '.password_confirmation' => 'required',
        ];
        return $result;
    }

    public function attributes()
    {
        $result = $this->editRequest->attributes();
        $result += [
            $this->language . '.first_name' => __('organizations.list.first_name'),
            $this->language . '.surname' => __('organizations.list.surname'),
            $this->language . '.email' => __('organizations.list.email'),
            $this->language . '.password' => __('organizations.list.password'),
            $this->language . '.password_confirmation' => __('organizations.list.password_confirm'),
        ];
        return $result;
    }
}
