<?php

namespace App\Http\Requests\Organizations;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class OrganizationsEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected $language;

    public function __construct()
    {

        $this->language = Auth::user()->languageValue->name;
    }

    public function authorize()
    {
        if(Auth::user()->hasAnyRole(['superadmin']) || Auth::user()->hasPermissionTo('branches-edit'))
            return true;
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(isset($this->organization) && !empty($this->organization))
            $this->language = $this->organization->language->name;
        return [
            $this->language.'.name' => 'required',
            'timezone_id' => 'required|exists:timezones,id',
        ];
    }

    public function attributes()
    {
        App::setLocale(Auth::user()->languageValue->name);
        return [
            $this->language . '.name' => __('organizations.list.name'),
           'timezone_id' => __('organizations.list.timezone_id'),
        ];
    }
}
