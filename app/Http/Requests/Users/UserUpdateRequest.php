<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Auth::user()->hasAnyRole(['superadmin']))
            return [
                'email' => ['required', 'email', Rule::unique('users', 'email')
                    ->whereNull('deleted_at')
                    ->ignore($this->user)],
                'password' => 'confirmed',
            ];
        $language = $this->user->language->name;
        $rules = [
            $language . '.surname' => 'required',
            $language . '.first_name' => 'required',

            'email' => ['required', 'email',
                Rule::unique('users', 'email')
                    ->whereNull('deleted_at')
                    ->ignore($this->user)],
            'password' => 'confirmed',
        ];
        if(Auth::user()->id != $this->user->id){
            $rules['status_user_id'] = 'required|exists:status_users,id';
            $rules['role_id'] = 'required|exists:roles,id';
        }

        return $rules;
    }

    public function attributes()
    {
        $language = Auth::user()->languageValue->name;
            return [
                $language . '.surname' => __('users.list.surname'),
                $language . '.first_name' => __('users.list.first_name'),
                'email' => __('users.list.email'),
                'password' =>  __('organizations.list.password'),
                'role_id' =>  __('users.list.roles'),
                'status_user_id' => __('users.list.status')
            ];
    }
}
