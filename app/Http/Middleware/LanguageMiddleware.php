<?php

namespace App\Http\Middleware;

use App\Models\System\Language;
use App\Services\Settings\SystemSettingService;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $language = Auth::check() ? Auth::user()->language_value_id : Session::get('language');

        if(empty($language))
            $language = Language::first();
        else
            $language = Language::find($language);

        /**Подминяем конфиг перевода datatable*/
        config(['datatables.lang' => '/vendor/datatables/'.$language->name.'/datatablesrus.json']);
        App::setLocale($language->name);

        $params = app(SystemSettingService::class)->load();
        if(!empty($params)){
            config([
                'adminlte.logo' => $params->paramTableOutput('system_name'),
                'adminlte.title' => $params->paramTableOutput('system_name'),
                'adminlte.logo_img' => $params->logo_photo(),
                'adminlte.logo_img_alt' =>$params->paramTableOutput('system_name'),
                'system_settings.auth_page_background' => $params->bg_photo(),
            ]);
        }
        return $next($request);
    }
}
