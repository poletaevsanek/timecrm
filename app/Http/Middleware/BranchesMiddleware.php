<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BranchesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(empty(Auth::user()->branch_id) && Auth::user()->organization->branch)
            return redirect()->route('branches.index');
        if(Auth::user()->company()->roles->isEmpty() && !Auth::user()->hasAnyRole('superadmin'))
            return redirect()->route('roles.index');
        return $next($request);
    }
}
