<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\Settings\SystemSettingEditRequest;
use App\Models\Settings\SystemSetting;
use Illuminate\Http\Request;
use App\Services\Settings\SystemSettingService as Service;

class SystemSettingController extends Controller
{
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function edit(SystemSetting $systemSetting)
    {
        return $this->editElement($systemSetting);
    }

    public function update(SystemSettingEditRequest $request, SystemSetting $systemSetting)
    {
        if(request()->ajax()){
            return $this->service->responseSuccess();
        }
        return $this->updateElement($request, $systemSetting);
    }
}
