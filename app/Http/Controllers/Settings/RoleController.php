<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\Settings\RoleEditRequest;
use Illuminate\Http\Request;
use App\Services\Settings\RoleService as Service;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function __construct(Service $service)
    {
        $this->service = $service;

        $this->middleware('can:roles-read' , ['only' => ['index']]);
        $this->middleware('can:roles-edit' , ['only' => ['edit', 'update', 'create', 'store']]);
        $this->middleware('can:roles-delete' , ['only' => ['destroy']]);
    }

    public function create()
    {
        return $this->createElement();
    }

    public function store(RoleEditRequest $request)
    {
        return $this->storeElement($request);
    }

    public function edit(Role $role)
    {
        return $this->editElement($role);
    }

    public function update(RoleEditRequest $request, Role $role)
    {
        return $this->updateElement($request, $role);
    }
}
