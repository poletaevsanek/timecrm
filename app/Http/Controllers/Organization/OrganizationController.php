<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Http\Requests\Organizations\OrganizationsEditRequest;
use App\Http\Requests\Organizations\OrganizationsStorageRequest;
use App\Models\Organization\Organization;
use Illuminate\Http\Request;
use App\Services\Organization\OrganizationService as Service;

class OrganizationController extends Controller
{
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function create()
    {
        return $this->createElement();
    }

    public function store(OrganizationsStorageRequest $request)
    {
        if(request()->ajax()){
            return $this->service->responseSuccess();
        }
        return $this->storeElement($request);
    }

    public function edit(Organization $organization)
    {
        return $this->editElement($organization);
    }

    public function update(OrganizationsEditRequest $request, Organization $organization)
    {
        if(request()->ajax()){
            return $this->service->responseSuccess();
        }
        return $this->updateElement($request, $organization);
    }
}
