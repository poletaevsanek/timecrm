<?php

namespace App\Http\Controllers\Organization;

use App\Http\Controllers\Controller;
use App\Http\Requests\Organizations\OrganizationsEditRequest;
use App\Models\Organization\Branch;
use Illuminate\Http\Request;
use App\Services\Organization\BranchService as Service;
use Illuminate\Support\Facades\Auth;

class BranchController extends Controller
{
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function create()
    {
        return $this->createElement();
    }

    public function store(OrganizationsEditRequest $request)
    {
        if(request()->ajax()){
            return $this->service->responseSuccess();
        }
        return $this->storeElement($request);
    }

    public function edit(Branch $branch)
    {
        return $this->editElement($branch);
    }

    public function update(OrganizationsEditRequest $request, Branch $branch)
    {
        if(request()->ajax()){
            return $this->service->responseSuccess();
        }
        return $this->updateElement($request, $branch);
    }

    public function editBrach($id)
    {
        Auth::user()->branch_id = $id;
        Auth::user()->save();
        return redirect()->back();
    }
}
