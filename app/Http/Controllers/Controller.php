<?php

namespace App\Http\Controllers;

use App\Helpers\ToasterHelper;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ToasterHelper;

    public $service;

    public function index()
    {
        if (request()->ajax()) {
            return $this->service->getDatatableElements();
        }
        return view($this->service->templatePath . 'index')->with($this->service->outputData());
    }

    public function createElement()
    {
        $with = $this->service->editElement();
        return view($this->service->templatePath . 'edit')->with($with);
    }

    public function editElement($element)
    {
        $with = $this->service->editElement($element);
        return view($this->service->templatePath . 'edit')->with($with);
    }

    public function storeElement($request)
    {
        $attribute = $this->service->storeElement($request);
        if(!$attribute)
            return $this->errorfulElementUpdate();
        return $this->successStore($attribute['name'], $attribute['route']);
    }

    public function updateElement($request, $model)
    {
        $attribute = $this->service->updateElement($model, $request);
        if(!$attribute)
            return $this->errorfulElementUpdate();
        return $this->successUpdate($attribute['name'], $attribute['route']);
    }

    public function destroy($id)
    {
        if(request()->ajax()) {
            $result = $this->service->destroyElement($id);
            if($result)
                return response()->json([
                    'status' => 'success',
                    //'message' => 'Элемент удален',
                    'title' => 'Информация',
                ]);
        }
        return response()->json([
            'status' => 'false'
        ]);
    }

//    public function upload(Request $request)
//    {
//        $data = $this->service->upload($request);
//        return response()->json([
//            'status' => 'success',
//            'file_name' => $data->file_name,
//            'path' => $data->getUrl(),
//        ]);
//    }
//
//    public function deleteDocument(Request $request)
//    {
//        $data = $this->service->deleteDocument($request);
//        return response()->json([
//            'status' => 'success'
//        ]);
//    }
}
