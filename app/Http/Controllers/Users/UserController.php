<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UserStoreRequest;
use App\Http\Requests\Users\UserUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Services\Users\UserService as Service;

class UserController extends Controller
{
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function create()
    {
        return $this->createElement();
    }

    public function store(UserStoreRequest $request)
    {
        if(request()->ajax()){
            return $this->service->responseSuccess();
        }
        return $this->storeElement($request);
    }

    public function edit(User $user)
    {
        return $this->editElement($user);
    }

    public function update(User $user, UserUpdateRequest $request)
    {
        if(request()->ajax()){
            return $this->service->responseSuccess();
        }
        return $this->updateElement($request, $user);
    }
}
