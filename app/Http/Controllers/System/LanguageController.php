<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use App\Services\LanguageService as Service;
use Illuminate\Http\Request;

class LanguageController extends Controller
{

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function edit($language)
    {
        $this->service->editLanguage($language);
        return redirect()->back();
    }
}
