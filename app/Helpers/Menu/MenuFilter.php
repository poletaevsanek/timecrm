<?php

namespace App\Helpers\Menu;

use App\Models\System\Language;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;

class MenuFilter implements FilterInterface
{
    public function transform($item)
    {
        /**
         * Получаем языки из бд. Формируем submenu для header adminlte
         */
        if(isset($item['branches']) && Auth::user()->organization->branch){
            $branches = Auth::user()->organization->branches()->whereNull('parent_id')->get();
            $item['text'] =  Auth::user()->company()->paramTableOutput('name');
            $item['submenu'] = [];
            foreach ($branches as $branch){
                $item['submenu'][] = [
                    'text' => $branch->paramTableOutput('name'),
                    'href' => route('branches.editBranch', $branch->id),
                    'url' => route('branches.editBranch', $branch->id),
                    'class' =>  Auth::user()->branch_id == $branch->id ? 'active' : ''
                ];
            }
        }
        if(isset($item['language'])){
            /**
             * Язык выбранный у пользователя или сохраненный в сессии
             */
            $language = Auth::check() ? Auth::user()->language_value_id : Session::get('language');

            if(empty($language))
                $language = Language::first();
            else
                $language = Language::find($language);

            $languageId = $language->id;
            $item['text'] = view('crm.language.menu-item-language', compact('language'));
            $item['submenu'] = [];
            $languages = Language::all();
            /**
             * Формирование списка всех языков из БД
             */
            foreach ($languages as $language){
                $item['submenu'][] = [
                    'text' => view('crm.language.menu-item-language', compact('language')),
                    'href' => '#',
                    'url' => '#',
                    'data-compiled' => 'onclick="Main.editLanguage(\''.route('language.edit', $language->id).'\')"',
                    'class' => $languageId == $language->id ? 'active' : ''
                ];
            }
        }
        return $item;
    }

}
