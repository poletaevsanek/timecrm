<?php

namespace App\Helpers;

trait ToasterHelper
{

    public function successStore($attribute, $route)
    {
        toastr()->success(__('general.message.store.body', [
            'attribute' => $attribute,
        ]), __('general.message.store.title'));
        return redirect($route);
    }

    public function successUpdate($attribute, $route)
    {
        toastr()->success(__('general.message.update.body', [
            'attribute' => $attribute,
        ]), __('general.message.update.title'));
        return redirect($route);
    }

    public function errorfulElementUpdate()
    {
        toastr()->error(__('general.message.error.body'), __('general.message.error.title'));
        return redirect()->back();
    }
}
