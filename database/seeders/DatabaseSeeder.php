<?php

namespace Database\Seeders;

use Database\Seeders\System\LanguageTableSeeder;
use Database\Seeders\System\PermissionTableSeeder;
use Database\Seeders\System\StatusUserSeeder;
use Database\Seeders\System\SystemSettingTableSeeder;
use Database\Seeders\System\TimezonesTableSeeder;
use Database\Seeders\System\UserTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            LanguageTableSeeder::class,
            PermissionTableSeeder::class,
            StatusUserSeeder::class,
            UserTableSeeder::class,
            TimezonesTableSeeder::class,
            SystemSettingTableSeeder::class
        ]);
    }
}
