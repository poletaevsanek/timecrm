<?php

namespace Database\Seeders\System;

use App\Models\System\Timezone;
use Illuminate\Database\Seeder;

class TimezonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Timezone::firstOrCreate([
            'name' => 'AEST',
            'timezone' => 'UTC+10'
        ]);
        Timezone::firstOrCreate([
            'name' => 'AKT',
            'timezone' => 'UTC−9'
        ]);
        Timezone::firstOrCreate([
            'name' => 'AST',
            'timezone' => 'UTC-4'
        ]);
        Timezone::firstOrCreate([
            'name' => 'AWST',
            'timezone' => 'UTC+8'
        ]);
        Timezone::firstOrCreate([
            'name' => 'BDT',
            'timezone' => 'UTC+6'
        ]);
        Timezone::firstOrCreate([
            'name' => 'CAT',
            'timezone' => 'UTC+2'
        ]);
        Timezone::firstOrCreate([
            'name' => 'CET',
            'timezone' => 'UTC+1'
        ]);
        Timezone::firstOrCreate([
            'name' => 'CST',
            'timezone' => 'UTC−6'
        ]);
        Timezone::firstOrCreate([
            'name' => 'CXT',
            'timezone' => 'UTC+7'
        ]);
        Timezone::firstOrCreate([
            'name' => 'ChT',
            'timezone' => 'UTC−4'
        ]);
        Timezone::firstOrCreate([
            'name' => 'EST',
            'timezone' => 'UTC−5'
        ]);
        Timezone::firstOrCreate([
            'name' => 'GALT',
            'timezone' => 'UTC-6'
        ]);
        Timezone::firstOrCreate([
            'name' => 'HAST',
            'timezone' => 'UTC−10'
        ]);
        Timezone::firstOrCreate([
            'name' => 'JST',
            'timezone' => 'UTC+9'
        ]);
        Timezone::firstOrCreate([
            'name' => 'MT',
            'timezone' => 'UTC−7'
        ]);
        Timezone::firstOrCreate([
            'name' => 'MSK',
            'timezone' => 'UTC+3'
        ]);
        Timezone::firstOrCreate([
            'name' => 'NFT',
            'timezone' => 'UTC+11'
        ]);
        Timezone::firstOrCreate([
            'name' => 'PET',
            'timezone' => 'UTC-5'
        ]);
        Timezone::firstOrCreate([
            'name' => 'PKT',
            'timezone' => 'UTC+5'
        ]);
        Timezone::firstOrCreate([
            'name' => 'PMST',
            'timezone' => 'UTC-3'
        ]);
        Timezone::firstOrCreate([
            'name' => 'PST',
            'timezone' => 'UTC-8'
        ]);
        Timezone::firstOrCreate([
            'name' => 'ST',
            'timezone' => 'UTC−11'
        ]);
        Timezone::firstOrCreate([
            'name' => 'UTC',
            'timezone' => 'UTC'
        ]);
    }
}
