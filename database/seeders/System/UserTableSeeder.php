<?php

namespace Database\Seeders\System;

use App\Models\System\Language;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::updateOrCreate([
            'email' => 'admin@admin.com'
        ],[
            'surname' => 'admin',
            'first_name' => 'admin',
            'middle_name' => 'admin',

            'password' => Hash::make('welcome'),
            'language_id' => Language::first()->id,
            'language_value_id' => Language::first()->id,
            'parent_id' => null
        ]);
        $user->assignRole('superadmin');
    }
}
