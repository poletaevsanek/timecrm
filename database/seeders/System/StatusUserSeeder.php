<?php

namespace Database\Seeders\System;

use App\Models\System\Language;
use App\Models\System\StatusUser;
use Illuminate\Database\Seeder;

class StatusUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusLanguage = [
            'active' => [
                'ru' => 'Активен',
                'en' => 'Active',
                'fr' => 'Actif'
            ],
            'not_active' => [
                'ru' => 'Не активен',
                'en' => 'Not active',
                'fr' => 'Pas actif'
            ],
        ];
        $ruLanguage = Language::whereName('ru')->first();
        $allLanguage = Language::all();
        $active = $ruLanguage->statusUsers()->firstOrCreate(['name' => 'Активен']);
        $not_active = $ruLanguage->statusUsers()->firstOrCreate(['name' => 'Не активен']);
        foreach ($allLanguage as $item){
            if($ruLanguage->id != $item->id){
                $active->translations()->firstOrCreate([
                    'name' => $statusLanguage['active'][$item->name],
                    'language_id' => $item->id
                ]);
                $not_active->translations()->firstOrCreate([
                    'name' => $statusLanguage['not_active'][$item->name],
                    'language_id' => $item->id
                ]);

            }

        }
    }
}
