<?php

namespace Database\Seeders\System;

use App\Models\Settings\SystemSetting;
use App\Models\System\Language;
use Illuminate\Database\Seeder;

class SystemSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(SystemSetting::get()->isEmpty()) {
            $allLanguageSeeder = Language::all();
            $data = [
                'system_name' => 'Time-CRM',
                'logo' => 0,
                'background_logo' => 0,
                'language_id' => $allLanguageSeeder->first()->id
            ];
            $model = SystemSetting::create($data);
            foreach ($allLanguageSeeder as $item) {
                if($item->id != $model->language_id){
                    $data['language_id'] = $item->id;
                    $model->translations()->create($data);
                }
            }
        }
    }
}
