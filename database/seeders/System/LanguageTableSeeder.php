<?php

namespace Database\Seeders\System;

use App\Models\System\Language;
use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Language::firstOrCreate([
            'name' => 'ru'
        ],[
            'icon' => '\img\ru.png'
        ]);
        Language::firstOrCreate([
            'name' => 'en'
        ],[
            'icon' => '\img\en.png'
        ]);
        Language::firstOrCreate([
            'name' => 'fr'
        ],[
            'icon' => '\img\fr.png'
        ]);
    }
}
