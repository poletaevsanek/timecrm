<?php

namespace Database\Seeders\System;

use App\Models\RoleSystem;
use App\Models\System\Language;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $config =config('roles');
        foreach ($config['permissions'] as $key => $item){
            foreach (explode(',', $item) as $p => $perm) {
                $name = $key . '-' . $config['permissions_map'][$perm];
                Permission::firstOrCreate(['name' => $name]);
            }
        }
        $ruLanguage = Language::whereName('ru')->first();
        $allLanguage = Language::all();
        foreach ($config['roles'] as $roleName => $permission) {
            $role = $ruLanguage->roles()->firstOrCreate([
                'name' => $roleName,
                'display_name' => $config['translation'][$roleName]['ru']
            ]);
            foreach ($allLanguage as $language){
                if($language->id != $ruLanguage->id)
                    $role->translations()->firstOrCreate([
                        'name' => $config['translation'][$roleName][$language->name],
                        'language_id' => $language->id
                    ]);
            }
            foreach ($permission as $key => $item){
                foreach (explode(',', $item) as $p => $perm) {
                    $name = $key . '-' . $config['permissions_map'][$perm];
                    $newPermission = Permission::firstOrCreate(['name' => $name]);
                    $role->givePermissionTo($newPermission);
                }
            }
        }
    }
}
