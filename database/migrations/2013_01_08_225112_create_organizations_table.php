<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('logo')->default(0)->comment('Логотип компании');
            $table->boolean('branch')->default(0)->comment('
               0-нет филиалов, 1-есть филиалы
            ');
            $table->unsignedInteger('language_id')->comment('Язык строки');
            $table->unsignedInteger('timezone_id')->nullable();
            $table->unsignedInteger('parent_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('language_id')->references('id')
                ->on('languages');
            $table->foreign('timezone_id')->references('id')
                ->on('timezones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
