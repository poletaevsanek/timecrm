<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('surname');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('email')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->dateTime('date_of_birth')->nullable()->comment('Дата рождения');
            $table->dateTime('start_date_job')->nullable()->comment('Дата принятия на работу');
            $table->string('password');
            $table->boolean('avatar')->default(0)->comment('Аватар');
            $table->text('address')->nullable();
            $table->longText('about_me')->nullable()->comment('О себе');

            $table->unsignedInteger('status_user_id')->default(1);
            $table->unsignedInteger('organization_id')
                ->nullable();
            $table->unsignedInteger('branch_id')
                ->nullable();
            $table->unsignedInteger('language_id')->comment('язык данной строки');

            $table->unsignedInteger('language_value_id')->comment('Выбранный язык на конкретный момент');
            $table->unsignedInteger('parent_id')->nullable();

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('status_user_id')->references('id')
                ->on('status_users');

            $table->foreign('organization_id')->references('id')
                ->on('organizations');

            $table->foreign('branch_id')->references('id')
                ->on('branches');

            $table->foreign('language_id')->references('id')
                ->on('languages');

            $table->foreign('language_value_id')->references('id')
                ->on('languages');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
