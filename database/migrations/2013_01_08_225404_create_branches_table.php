<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->unsignedInteger('organization_id')->comment('организация');
            $table->unsignedInteger('language_id')->comment('Язык данной строки');
            $table->unsignedInteger('timezone_id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('organization_id')->references('id')
                ->on('organizations');

            $table->foreign('language_id')->references('id')
                ->on('languages');

            $table->foreign('timezone_id')->references('id')
                ->on('timezones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
