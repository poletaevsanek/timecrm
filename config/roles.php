<?php

return [
    'permissions' => [
        'organizations' => 'r,e,d',
        'users' => 'r,e,d',
        'branches' => 'r,e,d',
        'user_organization' => 'r,e',
        'settings' => 'r,e,d',
        'roles' => 'r,e,d',
        'all_settings' => 'r,e,d',
    ],
    'permissions_map' => [
        'r' => 'read',
        'e' => 'edit',
        'd' => 'delete'
    ],
    'roles' => [
        'superadmin' => [
            'organizations' => 'r,e,d',
            'user_organization' => 'r,e',
            'users' => 'e',
            'settings' => 'r,e,d',
        ],
        'admin_organization' => [
            'users' => 'r,e,d',
            'branches' => 'r,e,d',
            'all_settings' => 'r,e,d',
            'roles' => 'r,e,d'
        ],
    ],
    'translation' => [
        'superadmin' => [
            'ru' => 'Главный администратор',
            'en' => 'Chief administrator',
            'fr' => 'Super administrateur',
        ],
        'admin_organization' => [
            'ru' => 'Администратор организации',
            'en' => 'Organization administrator',
            'fr' => 'Administrateur de l\'organisation',
        ],
    ],
];
