var Main = {
    deleteMethodLE: function (urlDestroy, elementName, usFunc, table) {
        if (!table) {
            table = window.LaravelDataTables["dataTableBuilder"];
        }
        smoke.confirm(notificationList['delete'], function (result) {
            if (result === false) return; //Выбрали отмена
            var data = new FormData();
            data.append('_token', window.token);
            data.append('_method', 'DELETE');
            $.ajax({
                url: urlDestroy,
                type: "POST",
                cache: false,
                data: data,
                processData: false,
                contentType: false,
                success: function (arrayData) {
                    if (arrayData['status'] && arrayData['status'] !== 'false') {
                        toastrConfig.info(arrayData['message'], arrayData['title']);
                        if (usFunc == null)
                            table.ajax.reload(null, false);
                        else
                            location.href = usFunc;
                    } else {
                        toastrConfig.warning(notificationList['error_body'], notificationList['error_title'])
                    }
                },
                error: function (jqXHR, exception) {
                    toastrConfig.warning(notificationList['error_body'], notificationList['error_title'])
                }
            });
        }, {
            ok: notificationList['smoke_pure_yes'],
            cancel: notificationList['smoke_pure_no']
        });
    },

    editLanguage: function (url) {
        var my_str_value = $('[role="form"]').attr('data-attr-lang');
        console.log(my_str_value);
        if (typeof my_str_value !== typeof undefined && my_str_value !== false) {
            smoke.confirm(notificationList['editLanguage'], function (result) {
                if (result === false) return; //Выбрали отмена
                window.location.href = url;
            }, {
                ok: notificationList['smoke_pure_yes'],
                cancel: notificationList['smoke_pure_no']
            });
        } else {
            window.location.href = url;
        }
    },

    initSelect2: function () {
        $.each($('.select2'), function () {
            $(this).select2({
                allowClear: true,
                language: Translation.selectTranslation($(this).attr('data-language')),
            });
        })
    },

    initDatePicker: function (element) {
        let language = $('#' + element).data('language');
        if (language != 'en')
            language += '-' + language
        else
            language = 'en-us'

        $('#' + element).datepicker({
            format: 'dd.mm.yyyy',
            uiLibrary: 'bootstrap4',
            locale: language,
        });
    },

    initPhoneMask: function () {
        $('.phone').inputmask("+9(999) 999-99-99");
        return true;
    },

    sendFormData: function (event, form, method = 'submit', successEvent = function (arrayData) {
        if (method == 'submit') {
            form.removeAttr('onsubmit');
            form.submit();
        } else {
            toastrConfig.success('Изменения сохранены');
        }
    }) {
        event.preventDefault();
        let url = form.attr('action');
        let dataFormat = new FormData(form[0]);
        form.find('.is-invalid').removeClass('is-invalid');

        var errorHandle = function (jqXHR, textStatus, errorThrown) {
            var data = jqXHR.responseJSON;

            if (data['errors'] && data['message']) {
                $.each(data['errors'], function (index, value) {
                    toastrConfig.error(value[0], 'Ошибка');
                    index = index.split('.');
                    let name = index[0];
                    $.each(index, function (key, item) {
                        if (key != 0)
                            name += '[' + item + ']'
                    });
                    $('[name="' + name + '"').addClass('is-invalid');
                });
                return true;
            }
        };

        Main.ajaxRequest('POST', url, dataFormat, successEvent, errorHandle);
    },

    ajaxRequest: function (typeRequest, urlRequest, dataRequest, successFunction, errorFunction, returnFunction) {
        var request = $.ajax({
            type: typeRequest,
            url: urlRequest,
            cache: false,
            data: dataRequest,
            processData: false,
            contentType: false,
            success: successFunction,
            error: errorFunction
        });
        if (returnFunction === true)
            return request;
        else
            return true;
    },

    readURL: function (input, attr = 'data-logo') {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('[' + attr + '="true"]').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
};

var toastrConfig = {
    warning: function (text, title = '') {
        toastr.warning(text, title, {timeOut: 5000})
    },
    success: function (text, title = '') {
        toastr.success(text, title, {timeOut: 5000})
    },
    error: function (text, title = '') {
        toastr.error(text, title, {timeOut: 5000})
    },
    info: function (text, title = '') {
        toastr.info(text, title, {timeOut: 5000})
    },
};

$(function () {
    Main.initSelect2();
    Main.initPhoneMask();
    $('[data-date="true"]').each(function (key, value) {
        Main.initDatePicker($(this).attr('id'))
    });
});
